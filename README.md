# Trianga's Project: Battle Splash 2.0 Modding Tools

This repository will provide necessary tools for modding **Trianga's Project: Battle Splash 2.0**.


## Getting Started

These steps will guide you through the installation of the modding tools.

### Requirements/Prerequisites

To start modding Battle Splash, the following software(s) is (are) needed:

- **Windows 7 SP1+, 8, 10, 64-bit versions only; Mac OS X 10.9+.**

- **Unity Editor 2017.2.0f3**:

  - [Unity Hub](https://unity3d.com/get-unity/download) (select **Download Unity Hub**)
  - **or**
  - [Unity 2017.2.0f3 direct download](https://unity3d.com/unity/whats-new/unity-2017.2.0)

>**NOTE:** The Unity version MUST be **2017.2.0f3**. Any higher releases are not supported, any lower releases will partially work (that is, inside 2017.2 branch).

- **3d software (optional):** Blender, 3dsMax, Maya, or any software that Unity supports and/or be able to export FBX.

- **Unity knowledge (optional):** It's recommended that you walkthrough [Unity's official tutorial](https://learn.unity.com/) to get some basic ideas about game creation in Unity, and how the Unity Editor itself works.

### Installing

You'll need to install Unity Editor with the EXACT version 2017.2.0. There're two possible ways to install Unity:

#### Via **Unity Hub** (Recommended)

 Unity Hub makes downloading and managing Unity Version/Unity Project easier without having to worry about many underlying technical details.

   - Download Unity Hub and install it.
   - Open Unity Hub.
   - Login/Create account with Unity Account (if asked).
   - Select tab **Install** on the left.
   - Click **Add**.
   - Select [**download archive**](https://unity3d.com/get-unity/download/archive).
   - Click on **Unity 2017.x**.
   - Click on **Unity Hub** next to **Unity 2017.2.0** (released on *12 Oct, 2017*).
   - You will be asked to open link with Unity Hub. Press Open.
   - Unity Hub will Open the Add module screen. Here you can select additional installations for Unity. It's recommended to install **Documentation** as each version might differ from each other.
   - Click **Install**.

#### Via **direct download** (Advanced)

Choose this approach if you prefer self-managing your own application on your machine.

   - Visit [Unity 2017.2.0f3 download page](https://unity3d.com/unity/whats-new/unity-2017.2.0).
   - Depending on your OS (Windows/Mac), scroll down and select **Unity Editor**.
     - > **NOTE**: for macOS, you will need to download *Windows Target Support* as well.
   - (Optional/Recommended) Select **Documentation** to download Unity 2017.2.0f3 documentation.
   - Install **Unity Editor** first.
   - Install the rest of downloaded components.

### Quick Start

#### 1. Downloading the Tools

Head over to [Releases page](https://gitlab.com/mnbh1c10/battle-splash-modding/-/releases) to download the latest release (***.unitypackage** file) of the Modding Tool.

#### 2. Creating an empty project

If you use **Unity Hub:**

- Select Tab **Projects**
- Click the **New** button (or click the arrow down button if you have more than one version of Unity, and then select Unity 2017.2.0f3).
- Select project type as 3D, type in the project name, select the project's location.
- Click **Create**.
  
If you use **Unity 2017.2.0f3 directly:**

- Click **New**.
- Select project type as 3D, type in the project name, select the project's location.
- Click **Create Project**
  
#### 3. Importing the modding tools

After creating the project, simply drag and drop the ***.unitypackage** file into the **Project** Windows and press **Import**.

#### 4. Making the First Trianga's mod

- In the **Project** window, navigate to **Assets/Battle Splash Modding/Prefab**, and select **Ethan**
- In the preview window (under **Inspector** window), click on **none** next to **AssetBundle**, select **New...* and type in any name you like for the mod. (for example: *bs-mod-ethan*)
- On the menu bar, click on **Window/Battle Splash/Content Extension/Bundle Builder**.
- You should be able to see your mod name in the openned window (for the previous example, *bs-mod-ethan* should be there.)
  - > **PRO TIPS:** you can click **Details** to see what assets will be included in the associalted mod name.
- Click **Build Bundle -> Yes, build now**, and wait for Unity's magic.
- If there's nothing wrong, a dialog will show "Operation completed".
- Check the **Project** window, you should be able to see the folder **BSAssetsBundle/** there.
  - > **NOTE:** sometimes Unity won't be able to regconize the folder right away, but it's still be there. You can either Alt+TAB or navigate directly to your project's Assets folder for the files.
- Navigate to **<Your project name>/Assets/BSAssetsBundle/**, you should see a folder there.
- Launch Battle Splash. Go to **Options->Game**, make sure **Modding** is **enabled**.
  - > **NOTE:** If *Modding* is *disabled*, turn it to *enable*, and **restart the game**. This will force Battle Splash to generate correct modding folder for us.
- Navigate to **<Battle Splash's installation folder>/Modding/WindowsPlayer** and then paste everything in **<Your project name>/Assets/BSAssetsBundle/** there.
- Restart Battle Splash for the mod(s) to take effect.
- Play the game, select **Trianga** to see the **Magic**

#### 5. What's Next?
I'm currently writing a more detailed documentation about the modding process. However, you can go to the included scene **Character Setup Example** to get some basic details about modding. Additionally, if you can already started modding, you can reach me on the official [Battle Splash's Technical Support channel on Discord](https://discord.gg/BbMYkTn).