﻿using UnityEngine;
using UnityEngine.SceneManagement;

using UnityEditor;
using UnityEditor.SceneManagement;

using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.IO;

using BattleSplash.Modding;
using BattleSplash.Common;

public sealed class BSBundleBuilder : EditorWindow {


	public enum BuildMethod
	{
		Label,
		Selection,

	}

	[MenuItem ("Window/Battle Splash/Content Extension/Bundle Builder")]
	static void Init () {
		#pragma warning disable
		EditorWindow.GetWindow (typeof (BSBundleBuilder));
		#pragma warning restore

		InitDict ();

	}

	public static void InitDict ()
	{
		if (!dictBuildMethodDesc.ContainsKey (BuildMethod.Label))
			dictBuildMethodDesc.Add (BuildMethod.Label, "Build by Label will use the editor AssetBundle Label to build.");
		if (!dictBuildMethodDesc.ContainsKey (BuildMethod.Selection))
			dictBuildMethodDesc.Add (BuildMethod.Selection, "Build by Selection let you freely build your own selection of assets.");
	}

	[System.Serializable]
	public class ABLabelProperties
	{
		[System.Serializable]
		public class LevelInfoSerialized
		{
			public string path = "";
			public string description = "";
			public string imagePath = "";

			public LevelInfoSerialized ()
			{
				
			}

			public LevelInfoSerialized (string path,string desc,string imgPath)
			{
				this.path = path;
				this.description = desc;
				this.imagePath = imgPath;
			}
		}

		public bool isIncluded = true;
		public bool showAssetsPaths = false;
		public ModType modType = ModType.Custom;
		public Dictionary<string,LevelInfoSerialized> dictLevelInfo = new Dictionary<string, LevelInfoSerialized>();
	}

	[System.Serializable]
	public class ABBuildTargets 
	{
		public bool useCurrentPlatform = true;
		public bool windows = true;
		public bool osx = true;
		public bool linux = true;
	}

	void OnFocus ()
	{

		LoadFromPref ();
	}

	void OnSelectionChange()
	{
		Repaint();
	}

	public static string outputDirectory = "Assets/BSAssetsBundle/";
	private static readonly string bundleExtension = ".bsm";
	private static readonly string tempFolderPath = "[Temporary]BSBundleDirective";
	public static ABBuildTargets buildTargets = new ABBuildTargets ();

	public static BuildMethod buildMethod = BuildMethod.Label;

	/// <summary>
	/// Dictionary for Editor Asset Bundle Label inclusion.
	/// </summary>
	public static Dictionary<string,ABLabelProperties> dictLabel = new Dictionary<string, ABLabelProperties>();
	/// <summary>
	/// Description dictionary for BuildMethod
	/// </summary>
	public static Dictionary<BuildMethod,string> dictBuildMethodDesc = new Dictionary<BuildMethod, string> ();

	public static List<Object> listSelectedObject = new List<Object> ();

	static Vector2 scrollView = new Vector2 ();

	void OnGUI ()
	{
		if (dictBuildMethodDesc.Keys.Count == 0)
			InitDict ();


		//Define the selection state: 0 - no chance, 1 - select all, -1 - deselect all
		int selectionStateAll = 0;

		EditorGUILayout.HelpBox ("Bundle building options", MessageType.None);
		outputDirectory = EditorGUILayout.TextField ("Output Directory", outputDirectory);


		buildMethod = (BuildMethod)EditorGUILayout.EnumPopup (new GUIContent ("Build method", dictBuildMethodDesc [buildMethod]), buildMethod);

		buildTargets.useCurrentPlatform = EditorGUILayout.ToggleLeft ("Use current platform: " + EditorUserBuildSettings.activeBuildTarget.ToString (), buildTargets.useCurrentPlatform);
		if (!buildTargets.useCurrentPlatform) {
			EditorGUI.indentLevel++;
			buildTargets.windows = EditorGUILayout.ToggleLeft ("Windows", buildTargets.windows);
			buildTargets.osx = EditorGUILayout.ToggleLeft ("OSX Unversal", buildTargets.osx);
			buildTargets.linux = EditorGUILayout.ToggleLeft ("Linux Universal", buildTargets.linux);
			EditorGUI.indentLevel--;
		}
		switch (buildMethod)
		{
		//Build by Label =====================================================
		case BuildMethod.Label:
			EditorGUILayout.HelpBox ("Assets labels:", MessageType.None);

			EditorGUILayout.BeginHorizontal ();
			if (GUILayout.Button ("Select All")) {
				//Select all
				selectionStateAll = 1;
			}
			if (GUILayout.Button ("Deselect All")) {
				//Deselect all
				selectionStateAll = -1;
			}
			if (GUILayout.Button("Clear unused label"))
			{
				string[] unusedLabel = AssetDatabase.GetUnusedAssetBundleNames ();
				foreach (string ul in unusedLabel)
				{
					if (dictLabel.ContainsKey (ul))
						dictLabel.Remove (ul);
				}
				AssetDatabase.RemoveUnusedAssetBundleNames ();
			}
			GUILayout.FlexibleSpace ();
			EditorGUILayout.EndHorizontal ();

			scrollView = EditorGUILayout.BeginScrollView (scrollView);
			{
				string[] allLabels = AssetDatabase.GetAllAssetBundleNames ();
				for (int i = 0; i < allLabels.Length; i++) {
					string currentLabel = allLabels [i];
					if (!dictLabel.ContainsKey (currentLabel))
						dictLabel.Add (currentLabel, new ABLabelProperties ());

					EditorGUILayout.BeginHorizontal ();
					{
						
						if (selectionStateAll != 0)
							dictLabel [currentLabel].isIncluded = selectionStateAll == 1 ? true : false;
						dictLabel [currentLabel].isIncluded = EditorGUILayout.ToggleLeft (currentLabel, dictLabel [currentLabel].isIncluded);
						GUILayout.FlexibleSpace ();
						if (GUILayout.Button (dictLabel [currentLabel].showAssetsPaths ? "Close" : "Details"))
							dictLabel [currentLabel].showAssetsPaths = !dictLabel [currentLabel].showAssetsPaths;
						
					}
					EditorGUILayout.EndHorizontal ();

					if (dictLabel [currentLabel].showAssetsPaths) {
						EditorGUI.indentLevel++;
						string[] allAssetsPath = AssetDatabase.GetAssetPathsFromAssetBundle (currentLabel);
//						string output = "Asset Paths:\n\n";
						foreach (string s in allAssetsPath) {
//							output += s + "\n";

							if (Path.GetExtension (s).ToLower () == ".unity")
							{
								ABLabelProperties.LevelInfoSerialized levelInfo = null;
								if (dictLabel [currentLabel].dictLevelInfo == null)
									dictLabel [currentLabel].dictLevelInfo = new Dictionary<string, ABLabelProperties.LevelInfoSerialized> ();
								if (!dictLabel [currentLabel].dictLevelInfo.TryGetValue (s, out levelInfo)) {
									dictLabel [currentLabel].dictLevelInfo.Add (s, new ABLabelProperties.LevelInfoSerialized (s, "", ""));
									levelInfo = dictLabel [currentLabel].dictLevelInfo [s];
								}
								if (levelInfo == null) {
									levelInfo = new ABLabelProperties.LevelInfoSerialized (s, "", "");
									dictLabel [currentLabel].dictLevelInfo [s] = levelInfo;
								}

								EditorGUILayout.HelpBox ("Scene: " + levelInfo.path, MessageType.None);
								EditorGUI.indentLevel++;
								Sprite levelImage = null;
								levelImage = AssetDatabase.LoadAssetAtPath<Sprite> (levelInfo.imagePath);
								levelImage = (Sprite)EditorGUILayout.ObjectField ("Image:", levelImage, typeof(Sprite), false);
								if (levelImage) {
									levelInfo.imagePath = AssetDatabase.GetAssetPath (levelImage.GetInstanceID ());
								}

								levelInfo.description = EditorGUILayout.TextField ("Description", levelInfo.description);

								EditorGUI.indentLevel--;
							}
							else
							{
								EditorGUILayout.HelpBox ("Asset: " + s, MessageType.None);
							}
						}

						EditorGUI.indentLevel--;
//						EditorGUILayout.HelpBox (output, MessageType.Info);

					}

				}
			}

			EditorGUILayout.EndScrollView ();
			break;
		//Build by Selection =================================================
		case BuildMethod.Selection:
			

			break;
		}


		EditorGUILayout.Separator ();
		EditorGUILayout.BeginHorizontal ();
		{
			GUILayout.FlexibleSpace ();

			if (GUILayout.Button ("Build Bundle")) {
				string buildLocation = outputDirectory;

				string message = 
					"Please review your build:\n" + 
					"Build Location: " + buildLocation + "\n" +
					"Build method: " + buildMethod.ToString () + "\n";
				
				if (EditorUtility.DisplayDialog ("Ready to build.", message, "Yes, build now", "Hold on"))
				{
					if (buildTargets.useCurrentPlatform) {
						DoBuild (EditorUserBuildSettings.activeBuildTarget);
					} else {
						if (buildTargets.windows) {
							DoBuild (BuildTarget.StandaloneWindows);
						}
						if (buildTargets.osx) {
							DoBuild (BuildTarget.StandaloneOSXUniversal);
						}
						if (buildTargets.linux) {
							DoBuild (BuildTarget.StandaloneLinuxUniversal);
						}
					}
				}
			}
		}
		EditorGUILayout.EndHorizontal ();


		GUILayout.FlexibleSpace ();

		if (GUI.changed)
			SaveToPref ();
	}
		
	void SaveToPref ()
	{
		EditorPrefs.SetString ("BSBundleBuilder.buildTarget", Utils.ObjectToXMLString (buildTargets));
		EditorPrefs.SetString ("BSBundleBuilder.buildMethod", Utils.ObjectToXMLString (buildMethod));
		EditorPrefs.SetString ("BSBundleBuilder.outputPath", outputDirectory);
		EditorPrefs.SetString ("BSBundleBuilder.dictLabel", System.Convert.ToBase64String(Utils.ObjectToByteArray (dictLabel)));
	}

	void LoadFromPref ()
	{
		if (EditorPrefs.HasKey("BSBundleBuilder.buildTarget"))
		{
			buildTargets = Utils.XMLStringToObject<ABBuildTargets> (EditorPrefs.GetString ("BSBundleBuilder.buildTarget"));
		}
		if (EditorPrefs.HasKey("BSBundleBuilder.buildMethod"))
		{
			buildMethod = Utils.XMLStringToObject<BuildMethod> (EditorPrefs.GetString ("BSBundleBuilder.buildMethod"));
		}
		if (EditorPrefs.HasKey("BSBundleBuilder.outputPath"))
		{
			outputDirectory = EditorPrefs.GetString ("BSBundleBuilder.outputPath");
		}
		if (EditorPrefs.HasKey("BSBundleBuilder.dictLabel"))
		{
			dictLabel = (Dictionary<string,ABLabelProperties>)Utils.ByteArrayToObject (System.Convert.FromBase64String (EditorPrefs.GetString ("BSBundleBuilder.dictLabel")));
		}
	}

	void DoBuild (BuildTarget buildTarget)
	{
		string directiveFolder = Path.Combine ("Assets", tempFolderPath);

		Directory.CreateDirectory (directiveFolder);
		
		if (buildMethod == BuildMethod.Label) {
			
			string[] allBundleNames = AssetDatabase.GetAllAssetBundleNames ();
			string outputPath = Path.Combine (outputDirectory, buildTarget.ToString ());
			List<AssetBundleBuild> listBundle = new List<AssetBundleBuild> ();

			List<string> listDestroy = new List<string> ();

			foreach (string abName in allBundleNames) 
			{
				if (dictLabel [abName].isIncluded) {

					string bundleName = Path.Combine(Path.GetDirectoryName(abName),Path.GetFileNameWithoutExtension(abName));
					bundleName = bundleName.Replace(Path.DirectorySeparatorChar,'/');

					string bundleVariant = Path.GetExtension (abName).Replace(".","");

					bool isSceneAsset = false;

					if (!Directory.Exists (outputPath)) {
						try {
							Directory.CreateDirectory (outputPath);
						} catch (IOException ex) {
							Debug.LogError ("Cannot create directory:\n" + ex.ToString ());
							return;
						}
					}

					//Actual build-able list
					List<string> listAssetsToBuild = new List<string> ();
					//All specified assets
					List<string> assetCandidates = new List<string> (AssetDatabase.GetAssetPathsFromAssetBundle (abName));
					//All specifed scenes
					List<string> listScenesToBuild = assetCandidates.FindAll (item => Path.GetExtension (item).ToLower ().Equals (".unity"));
					isSceneAsset = listScenesToBuild.Count > 0;
					//All specified assembly
					List<string> listAssembly = assetCandidates.FindAll (item => Path.GetExtension (item).ToLower ().Equals (".dll"));



					//Add all associate asset that can be built into the directive.
					listAssetsToBuild.AddRange (isSceneAsset ? listScenesToBuild : assetCandidates);

					Debug.Log (abName + ":\n" + Path.DirectorySeparatorChar + "Assemblies: " + listAssembly.Count + " - Bundle name: " + bundleName + " - Variant: " + bundleVariant);

					//Only creat mod directive if there is no scene being built

					//Mod directive creation================
					string directivePath = Path.Combine (directiveFolder, abName);

					ModdingDirective modDirective = new GameObject (ModdingDirective.modDirectiveObjectName).AddComponent<ModdingDirective> ();

					//Filling the directive details
					modDirective.assemblies = listAssembly.ToArray ();
					modDirective.modType = dictLabel [abName].modType;

					//Create bundle Temp folder
					if (!Directory.Exists (directivePath))
						Directory.CreateDirectory (directivePath);

					//Find all included prefab
					List<Object> includedAssets = new List<Object> ();

					//Pre-processing directive========================
					foreach (string s in assetCandidates)
					{
						Object prefab = AssetDatabase.LoadAssetAtPath<Object> (s);
						if (prefab) {
							includedAssets.Add (prefab);
							//It's a GameObject prefab
							if (prefab.GetType () == typeof(GameObject))
							{
								GameObject go = (GameObject)prefab;

								//Try to find out the functionality of the mod
								ModdingBase prefabFunction = go.GetComponent<ModdingBase> ();
								if (prefabFunction)
								{
									if (typeof(CharacterRig).IsAssignableFrom (prefabFunction.GetType ())) {
										switch (prefabFunction.modTag) {
										case BattleSplashTag.Temiko:
											modDirective.Temiko_Replacement = go;
											break;
										case BattleSplashTag.Trianga:
											modDirective.Trianga_Replacement = go;
											break;
										case BattleSplashTag.Pentaga:
											modDirective.Pentaga_Replacement = go;
											break;
										case BattleSplashTag.Quadra:
											modDirective.Quadra_Replacement = go;
											break;
										}
									}
								}
							}
						}
					}

					//Prevent including another directive into the mod (only one directive per mod)
					includedAssets.RemoveAll (item => 
						item.GetType () == typeof(GameObject) ? ((GameObject)item).GetComponent<ModdingDirective> () : false);

					//Assign the included assets to the directive, so that they will be build with the scene.
					modDirective.assets = includedAssets.ToArray ();
					//=================================================

					if (!isSceneAsset) {
						//If a normal prefab asset mod
						//Prepare file name
						string modPath = Path.Combine (directivePath, ModdingDirective.modDirectiveObjectName + ".prefab");

						//Add directive to build directly
						listAssetsToBuild.Add (modPath);

						//TODO: A cheap workaround
						modPath = modPath.Replace ("\\", "/");
//						Debug.Log (System.Text.RegularExpressions.Regex.Escape(Path.DirectorySeparatorChar.ToString()) + ": " + modPath);

						//Create Directive asset.
						PrefabUtility.CreatePrefab (modPath,modDirective.gameObject);

						DestroyImmediate (modDirective.gameObject);

						//Add for destruction later
						listDestroy.Add (modPath);
					}
					else
					{
						//If a scene mod
						//Prepare scene file name
						string scenePath = Path.Combine (directivePath, ModdingDirective.sceneModDirectiveName + ".unity");

						//Create a new scene, put every prefab in it, close it, and add it to the build list

						//Addictively create a scene
						Scene directiveScene = EditorSceneManager.NewScene (NewSceneSetup.EmptyScene,NewSceneMode.Additive);
						//Pre-processing directive
//						foreach (string s in assetCandidates)
//						{
//							Object prefab = AssetDatabase.LoadAssetAtPath<Object> (s);
//							if (prefab)
//								includedAssets.Add (prefab);
//						}

						//TODO: Should be able to determine whether the mod is a map mod or complete game mod (?)
						modDirective.modType = ModType.Map;

						//Include Scene info into the bundle
						modDirective.scenes = new LevelInfo[listScenesToBuild.Count];
						for (int i = 0; i < listScenesToBuild.Count; i++)
						{
							ABLabelProperties.LevelInfoSerialized levelSerialized = dictLabel [abName].dictLevelInfo [listScenesToBuild [i]];
							Sprite levelSprite = AssetDatabase.LoadAssetAtPath<Sprite> (levelSerialized.imagePath);
							modDirective.scenes [i] = new LevelInfo (levelSerialized.path, levelSprite, levelSerialized.description, "");
						}

						//Finally, put the directive inside the map, so that it will be built.
						EditorSceneManager.MoveGameObjectToScene (modDirective.gameObject, directiveScene);
						//Save the scene
						EditorSceneManager.SaveScene (directiveScene, scenePath);
						//Close the scene after saving
						EditorSceneManager.CloseScene (directiveScene, true);

						//Add scene to build map
						listAssetsToBuild.Add (scenePath);

						//Add for destruction later
						listDestroy.Add (scenePath);
					}

					//=======================================

					//Create a asset bundle
					AssetBundleBuild bundle = new AssetBundleBuild ();
					bundle.assetBundleName = bundleName;
					bundle.assetBundleVariant = bundleVariant;

					bundle.assetNames = listAssetsToBuild.ToArray ();
					listBundle.Add (bundle);

				}
			}

			//Build all bundles
			AssetBundleManifest manifest = BuildPipeline.BuildAssetBundles (outputPath, listBundle.ToArray (), BuildAssetBundleOptions.ChunkBasedCompression, buildTarget);

			//Cleaning up the mess
			string[] allModDirective = listDestroy.ToArray ();
			for (int i = 0;i<allModDirective.Length;i++)
			{
//				Debug.Log ("Deleting: " + allModDirective [i]);
				AssetDatabase.DeleteAsset (allModDirective [i]);
//				DestroyImmediate (allModDirective [i]);
			}
			EditorUtility.DisplayDialog ("Build", manifest != null ? "Operation completed!" : "Failed to build. Please refer to the Console Log for more information.", "OK");
		}

		if (buildMethod == BuildMethod.Selection) {
			
		}

	}

}
