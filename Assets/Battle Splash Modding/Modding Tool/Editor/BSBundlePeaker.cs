﻿using UnityEngine;
using UnityEngine.SceneManagement;

using UnityEditor;
using UnityEditor.SceneManagement;

using System.Collections;
using System.Collections.Generic;

using System.IO;

using BattleSplash.Modding;


public class BSBundlePeaker : EditorWindow {

	public enum PeakingMode
	{
		Path,
		Object,
	}

	private static PeakingMode peakingMode = PeakingMode.Object;
	private static Object selectedObject;
	private static string message;

	private static List<AssetBundle> loadedBundle = new List<AssetBundle>();

	[MenuItem ("Window/Battle Splash/Content Extension/Bundle Peaker")]
	static void Init () {
		#pragma warning disable
		EditorWindow.GetWindow (typeof (BSBundlePeaker));
		#pragma warning restore
	}

	private WWW streamer = null;
	private bool streamerDisposed = true;

	IEnumerator LoadAndCheckDirective (string bundlePath)
	{
		
		System.Uri uri = new System.Uri (Path.Combine (Path.GetDirectoryName (Application.dataPath), bundlePath));
		string loadingURI = System.Uri.EscapeUriString (uri.AbsoluteUri);
		Debug.Log ("Loading from: " + loadingURI);

		streamer = new WWW (loadingURI);
		streamerDisposed = false;
		yield return streamer;
		if (streamer.error != null)
		{
			Debug.LogError (streamer.error);
			yield break;
		}

		AssetBundle bundle = streamer.assetBundle;
		loadedBundle.Add(bundle);

		if (bundle)
		{
			try 
			{
				List<string> assetPaths = new List<string> (bundle.GetAllAssetNames ());
				List<string> scenesPaths = new List<string> (bundle.GetAllScenePaths ());

				string directiveScenePath = scenesPaths.Find (item => item.Contains (ModdingDirective.sceneModDirectiveName));
				if (!string.IsNullOrEmpty (directiveScenePath)) {

					Debug.Log(bundle.name + "\n" + directiveScenePath);
					Scene activeScene = EditorSceneManager.GetActiveScene ();
					Scene directiveScene = EditorSceneManager.OpenScene (directiveScenePath, OpenSceneMode.Additive);
					ModdingDirective modDirective = FindObjectOfType<ModdingDirective> ();
					if (modDirective) {
						EditorSceneManager.MoveGameObjectToScene (modDirective.gameObject, activeScene);
					} else {
						message = "Cannot load the Modding Directive, file might be corrupt or unknow format.";
					}
					EditorSceneManager.CloseScene(directiveScene,true);

				}
				else
				{
					message = "No scene to load";
				}
			} 
			catch (System.Exception ex)
			{
				Debug.LogError (ex.ToString ());
				message = "Cannot load the directive. Please check console log for more information.";
			}
			bundle.Unload (false);
			DestroyImmediate (bundle);
		}
		streamer.Dispose ();
		streamerDisposed = true;
	}

	void ProgressBar (float value,string label) {
		// Get a rect for the progress bar using the same margins as a textfield:
		Rect rect = GUILayoutUtility.GetRect (18, 18, "TextField");
		EditorGUI.ProgressBar (rect, value, label);
		EditorGUILayout.Space ();
	}

	void OnGUI ()
	{
		peakingMode = (PeakingMode)EditorGUILayout.EnumPopup (new GUIContent("Peaking mode","Path mode allow peaking at a bundle outside the Assets folder.\nObject mode allow peaking at a bundle inside Assets folder."), peakingMode);
		switch (peakingMode)
		{
		case PeakingMode.Object:
			
			selectedObject = EditorGUILayout.ObjectField ("Bundle Object: ", selectedObject, typeof(Object), false);

			if (selectedObject) {
				string selectedObjectPath = AssetDatabase.GetAssetPath (selectedObject);
				EditorGUILayout.BeginHorizontal ();
				GUILayout.FlexibleSpace ();
				if (GUILayout.Button ("Check Content")) {
					AssetDatabase.RemoveUnusedAssetBundleNames ();
					AssetBundle bundle = AssetBundle.LoadFromFile (AssetDatabase.GetAssetPath (selectedObject));
					if (bundle != null) {
						string[] assetPaths = bundle.GetAllAssetNames ();
						string[] scenesPaths = bundle.GetAllScenePaths ();
						if (bundle.isStreamedSceneAssetBundle)
							message = "Scene paths:\n" + Utils.ArrayToString ("\n", scenesPaths);
						else
							message = "Asset Paths:\n" + Utils.ArrayToString ("\n", scenesPaths);
						bundle.Unload (true);
						DestroyImmediate (bundle);
					} else {
						message = "Not a valid Asset Bundle";
					}
				}
				if (GUILayout.Button ("Check directive")) {
					AssetDatabase.RemoveUnusedAssetBundleNames ();
					AssetBundle bundle = AssetBundle.LoadFromFile (selectedObjectPath);
					loadedBundle.Add (bundle);
					if (bundle != null) {
						
						message = "Loading";

						if (bundle.isStreamedSceneAssetBundle) {

							try 
							{
								List<string> assetPaths = new List<string> (bundle.GetAllAssetNames ());
								List<string> scenesPaths = new List<string> (bundle.GetAllScenePaths ());

								string directiveScenePath = scenesPaths.Find (item => item.Contains (ModdingDirective.sceneModDirectiveName));
								if (!string.IsNullOrEmpty (directiveScenePath)) {

									Debug.Log(bundle.name + "\n" + directiveScenePath);
									Scene activeScene = EditorSceneManager.GetActiveScene ();
									Scene directiveScene = EditorSceneManager.OpenScene (Path.GetFileNameWithoutExtension(directiveScenePath), OpenSceneMode.Additive);
									ModdingDirective modDirective = FindObjectOfType<ModdingDirective> ();
									if (modDirective) {
										EditorSceneManager.MoveGameObjectToScene (modDirective.gameObject, activeScene);
									} else {
										message = "Cannot load the Modding Directive, file might be corrupt or unknow format.";
									}
									EditorSceneManager.CloseScene(directiveScene,true);

								}
								else
								{
									message = "No scene to load";
								}
							} 
							catch (System.Exception ex)
							{
								Debug.LogError (ex.ToString ());
								message = "Cannot load the directive. Please check console log for more information.";
							}
							bundle.Unload (false);
							DestroyImmediate (bundle);


						} else {
							
						}
						
						if (bundle != null) {
							bundle.Unload (false);
							DestroyImmediate (bundle);
						}
					} else {
						message = "Not a valid Asset Bundle";
					}
				}
				EditorGUILayout.EndHorizontal ();

				EditorGUILayout.BeginHorizontal ();
				{
					GUILayout.FlexibleSpace ();
					if (GUILayout.Button ("Clear loaded bundle")) {
						foreach (AssetBundle b in loadedBundle) {
							if (b != null) {
								b.Unload (true);
								DestroyImmediate (b);
							}
						}
						loadedBundle.RemoveAll (item => item == null);
					}
				}
				EditorGUILayout.EndHorizontal ();
			} else {
				message = "No asset bundle specified.";
			}
			if (!streamerDisposed) {
				if (!streamer.isDone)
					ProgressBar (streamer.progress, "Loading..." + streamer.progress.ToString ("00.0%"));
			}
			EditorGUILayout.HelpBox (message, MessageType.Info);

			break;
		case PeakingMode.Path:
			
			break;
		}
	}
		
}
