﻿using UnityEngine;

using System.Collections;
using System.Collections.Generic;

//using BattleSplash.GameRule;
using BattleSplash.Common;

namespace BattleSplash.Modding
{
	/// <summary>
	/// Mod base type.
	/// </summary>
	public enum ModType
	{
		/// <summary>
		/// Universal mod
		/// </summary>
		Custom,
		/// <summary>
		/// Modding related to character
		/// </summary>
		Character,
		/// <summary>
		/// Modidng related to map
		/// </summary>
		Map,
		/// <summary>
		/// Modding related to weapon
		/// </summary>
		Weapon,
	}

	/*

	/// <summary>
	/// Mod functionality.
	/// </summary>
	public enum ModFunction
	{
		//Universal
		/// <summary>
		/// Custom modding function. This asset will be included into the Mod pack and will not be
		/// executed by Battle Splash.
		/// The Modder(s) is responsible for this asset's creation and function.
		/// </summary>
		Custom,

		//Character mod
		Character_Replacement,
		Character_Clothing,
		NewCharacter,

		//Animator mod
		Character_Animator,

	} */

	/// <summary>
	/// Mod bundle. This is an Asset Bundle wrapper for Battle Splash modding system.
	/// </summary>
	public sealed class ModBundle
	{
		public static implicit operator bool (ModBundle mod)
		{
			return mod != null;
		}

		//====================================
		public string modName;
		public string filePath;
		public bool loaded = false;
		public ModdingDirective directive
		{
			get
			{
				return _directive;
			}
			set
			{
				_directive = value;
				if (_directive) {
					if (_directive.modBundle != this) //Prevent recursive assignment
						_directive.modBundle = this;
				}
			}
		}
		private ModdingDirective _directive;

		public ModBundle (string name, string path)
		{
			this.modName = name;
			this.filePath = path;
		}

		public ModBundle () : this ("","")
		{

		}
	}

	/// <summary>
	/// Modding Directive acts as a Modding map for the game.
	/// </summary>
	public sealed class ModdingDirective : MonoBehaviour {

		public static ModdingDirective Find (System.Predicate<ModdingDirective> match)
		{
			return allModDirective.Find (match);
		}

		public static ModdingDirective[] allDirective
		{
			get
			{
				allModDirective.RemoveAll (item => !item);
				return allModDirective.ToArray ();
			}
		}
		private static List<ModdingDirective> allModDirective = new List<ModdingDirective> ();

		public static ModdingDirective[] GetDirectiveOfType(ModType modType)
		{
			return allModDirective.FindAll (item => item.modType == modType).ToArray ();
		}

		public const string sceneModDirectiveName = "[Directive]ModDirectiveScene";
		public const string modDirectiveObjectName = "modDirective";
		//===================================================================================
		public ModType modType = ModType.Custom;
		public ModBundle modBundle 
		{
			get
			{
				return _modBundle;
			}
			set
			{
				_modBundle = value;
				if (_modBundle != null) {
					if (_modBundle.directive  != this) //Prevent recursive assignment
						_modBundle.directive = this;
				}
			}
		}
		private ModBundle _modBundle = null;
		public LevelInfo[] scenes = new LevelInfo[]{ };

		/// <summary>
		/// The assemblies that will be included into the game.
		/// </summary>
		public string[] assemblies = new string[0];

		/// <summary>
		/// Additional assets for the mod - or - All assets that come along with the scene.
		/// </summary>
		public Object[] assets;

		//Character mod

		public GameObject Temiko_Replacement;
		public List<GameObject> Temiko_Clothing = new List<GameObject> ();
		public GameObject Trianga_Replacement;
		public List<GameObject> Trianga_Clothing = new List<GameObject> ();
		public GameObject Quadra_Replacement;
		public List<GameObject> Quadra_Clothing = new List<GameObject> ();
		public GameObject Pentaga_Replacement;
		public List<GameObject> Pentaga_Clothing = new List<GameObject> ();
		public List<GameObject> NewCharacter = new List<GameObject> ();

		//Animator mod
		public RuntimeAnimatorController Temiko_Animator;
		public RuntimeAnimatorController Trianga_Animator;
		public RuntimeAnimatorController Quadra_Animator;
		public RuntimeAnimatorController Pentaga_Animator;

		void Awake ()
		{
			allModDirective.Add (this);
		}

		void OnDestroy ()
		{
			allModDirective.RemoveAll (item => item == this || item == null);
		}
	}
}