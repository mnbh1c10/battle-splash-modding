﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

/// <summary>
/// Battle Splash's common.
/// </summary>
namespace BattleSplash.Common
{
	//===============Classes===============
	/// <summary>
	/// Extentions for enums.
	/// </summary>
	public static class EnumExtensions
	{
		/// <summary>
		/// A FX 3.5 way to mimic the FX4 "HasFlag" method. This method treat Zero value as Everything.
		/// </summary>
		/// <param name="variable">The tested enum.</param>
		/// <param name="value">The value to test.</param>
		/// <returns>True if the flag is set. Otherwise false.</returns>
		public static bool HasFlag(this System.Enum variable, System.Enum value)
		{
			// check if from the same type.
			if (variable.GetType() != value.GetType())
			{
				throw new System.ArgumentException("The checked flag is not from the same type as the checked variable.");
			}

			long num = System.Convert.ToInt64(value);
			long num2 = System.Convert.ToInt64(variable);

			return (num2 & num) == num;
		}

		/// <summary>
		/// A FX 3.5 way to mimic the FX4 "HasFlag" method. This method treat Zero value as Nothing.
		/// </summary>
		/// <param name="variable">The tested enum.</param>
		/// <param name="value">The value to test.</param>
		/// <returns>True if the flag is set. Otherwise false.</returns>
		public static bool HasFlagNoZero(this System.Enum variable, System.Enum value)
		{
			// check if from the same type.
			if (variable.GetType() != value.GetType())
			{
				throw new System.ArgumentException("The checked flag is not from the same type as the checked variable.");
			}

			long num = System.Convert.ToInt64(value);
			long num2 = System.Convert.ToInt64(variable);
			if (num == 0)
				return num2 == num;
			return (num2 & num) == num;
		}
	}

	/// <summary>
	/// Record Player's statistics.
	/// </summary>
	public struct PlayerStatistics
	{
		public float lifeTime;
		public float hurtTime;

		public void Reset ()
		{
			lifeTime = 0;
			hurtTime = 0;
		}

	}

	public static class GameInputName
	{
		public static string vertical = "Vertical";
		public static string horizontal = "Horizontal";
		public static string walk = "Walk";
		public static string jump = "Jump";
		public static string firePrimary = "Fire1";
		public static string fireSecondary = "Fire2";
		public static string reload = "Reload";
		public static string mouseScrollWheel = "Mouse ScrollWheel";
		public static string[] weapons = new string[] { "Weapon 1", "Weapon 2", "Weapon 3", "Weapon 4"};
		public static string item = "Item";
		public static string switchModule = "SwitchModule";

		public static string score = "Score";
	}



	public static class AnimatorTagHashes
	{
		public static int wet = Animator.StringToHash (CharacterAnimatorTag.Wet.ToString ());
		public static int bolt = Animator.StringToHash (CharacterAnimatorTag.Bolt.ToString ());
		public static int leftHand = Animator.StringToHash (CharacterAnimatorTag.LeftHand.ToString ());
		public static int landing = Animator.StringToHash (CharacterAnimatorTag.Landing.ToString ());
		public static int reload = Animator.StringToHash (CharacterAnimatorTag.Reload.ToString ());
		public static int noLookAt = Animator.StringToHash (CharacterAnimatorTag.NoLookAt.ToString ());
		public static int Switch = Animator.StringToHash (CharacterAnimatorTag.Switch.ToString ());
		public static int shoot = Animator.StringToHash (CharacterAnimatorTag.Shoot.ToString ());
		public static int reset = Animator.StringToHash (CharacterAnimatorTag.Reset.ToString ());
		public static int jumping = Animator.StringToHash (CharacterAnimatorTag.Jumping.ToString ());

		static AnimatorTagHashes ()
		{
			Init ();
		}

		public static bool IsInited
		{
			get
			{
				return isInited;
			}
		}
		private static bool isInited = false;

		public static void Init ()
		{
			if (!isInited)
			{
				isInited = true;
				wet = Animator.StringToHash (CharacterAnimatorTag.Wet.ToString ());
//				Debug.Log ("Wet: " + wet);
				bolt = Animator.StringToHash (CharacterAnimatorTag.Bolt.ToString ());
//				Debug.Log ("Bolt: " + bolt);
				leftHand = Animator.StringToHash (CharacterAnimatorTag.LeftHand.ToString ());
//				Debug.Log ("Left Hand: " + leftHand);
				landing = Animator.StringToHash (CharacterAnimatorTag.Landing.ToString ());
//				Debug.Log ("Landing: " + landing);
				reload = Animator.StringToHash (CharacterAnimatorTag.Reload.ToString ());
//				Debug.Log ("Reload: " + reload);
				noLookAt = Animator.StringToHash (CharacterAnimatorTag.NoLookAt.ToString ());
//				Debug.Log ("No Look At: " + noLookAt);
				Switch = Animator.StringToHash (CharacterAnimatorTag.Switch.ToString ());
//				Debug.Log ("Switch: " + Switch);
				shoot = Animator.StringToHash (CharacterAnimatorTag.Shoot.ToString ());
//				Debug.Log ("shoot: " + shoot);
				reset = Animator.StringToHash (CharacterAnimatorTag.Reset.ToString ());
//				Debug.Log ("reset: " + reset);
				jumping = Animator.StringToHash (CharacterAnimatorTag.Jumping.ToString ());
//				Debug.Log ("jumping: " + jumping);
			}
		}
	}


	//===========Tags=====================

	//Battle Splash Tag for modding and Identifying
	public enum BattleSplashTag
	{
		Other,
		Temiko,
		Trianga,
		Pentaga,
		Quadra,
		SBot,
		Player,
		NPC,
		SniperRifle,
		Bazooka,
		Pistol,
		Balloon,
		Pole,
		Turret,
		DigitalShield,
		DiskDryer,
	}

	public enum CharacterAnimatorTag
	{
		Wet,
		Bolt,
		LeftHand,
		Landing,
		Reload,
		NoLookAt,
		Switch,
		Shoot,
		Reset,
		Jumping,
	}

	//Camera Enum===================================

	public enum CameraMode
	{
		ThirdPersonShooter,
		FirstPersonShooterGeneric,
		FirstPersonShooterImmersive,
	}

	//Networking============================
	public enum NETGROUP
	{
		PLAYER,
		NETSTARTER,
		ITEM,
		NPC,
		NETPLAYERVIEWER,
	}

	public enum ServerHostType
	{
		Public,
		FriendsOnly,
		Private,
	}

	//Character enum=================================
	public enum HeadControlMode
	{
		SelfImplement,
		AnimatorLayer,
		AnimatorLookAt,
	}

	public enum EquipmentPosition
	{
		Hips,
		Chest,
		LongWeapon,
		Sword,
		ArmShield,
		LeftGun,
		RightGun,
		Grenade,
		Magazine,
	}

	//AI Enums================================

	public enum AI_Level
	{
		Weak,
		Regular,
		Hard,
		Expert,
	}

	public enum AI_ACTION_MODE
	{
		STOP,
		MOVE_POSITION,
		RANDOM_PATH,
		FOLLOW,
		FLEE,
		ATTACK,
		UNSTUCK,
	}

	//Special Gameplay Effects==========================

	public interface IEventEffects
	{
		void OnEvent ();
	}


	//Level========================================
	[System.Serializable]
	public class LevelInfo
	{

		public string name = "";
		public bool isAvailable = true;
		/// <summary>
		/// Path to the bundle that this scene belongs to. Empty means this is an interal scene.
		/// </summary>
		[Tooltip("Path to the bundle that this scene belongs to. Empty means this is an interal scene.")]
		public string bundlePath = "";
		public UnityEngine.SceneManagement.Scene scene;
		public Sprite image = null;
		[TextArea]
		public string description = "";

		public LevelInfo()
		{

		}

		public LevelInfo(LevelInfo other) 
		{
			CopyFrom (other);
		}

		public void CopyFrom (LevelInfo other)
		{
			SetValues (other.name, other.image, other.description, other.bundlePath, other.isAvailable);
		}

		public LevelInfo (string name, Sprite image, string desc, string bundle, bool isAvailable = true) 
		{
			SetValues (name, image, desc, bundle, isAvailable);
		}

		private void SetValues (string name, Sprite image, string desc, string bundle, bool isAvailable = true) 
		{
			this.name = name;
			if (name.ToLower().Contains(".unity"))
				scene = SceneManager.GetSceneByPath(name);
			else
				scene = SceneManager.GetSceneByName(name);
			this.image = image;
			this.description = desc;
			this.bundlePath = bundle;
			this.isAvailable = isAvailable;
		}

		public LevelInfo (string name, bool isAvailable = true) : this (name, null, "", "", isAvailable) {}

		public LevelInfo (string name, Sprite image, bool isAvailable = true) : this (name, image, "", "", isAvailable) {}

		public LevelInfo (string name, string desc, bool isAvailable = true) : this (name, null, desc, "", isAvailable) {}

		public LevelInfo (string name, string desc, string bundle, bool isAvailable = true) : this (name, null, desc, bundle, isAvailable) {}

		public LevelInfo (string name, Sprite image, string desc, bool isAvailable = true) : this (name, image, desc, "", isAvailable) {}


	}

	//Helper Enums================================================
	public enum Axis
	{
		X,
		Y,
		Z,
		NegativeX,
		NegativeY,
		NegativeZ
	}


	//Steam Lobby Enums============================================

	public enum DistanceFilter
	{
		Close,
		Default,
		Far,
		Worldwide,
	}


	//Classes======================================================

	public class LobbySearchFilter
	{
		[CustomAttributes.FieldMetaData ("Distance Filter")]
		public DistanceFilter distanceFilter = DistanceFilter.Default;
	}



	//Audio--------------------------------------

	[System.Serializable]
	public struct CharacterVoices
	{
		public AudioClip[] voiceSpawn;
		public AudioClip[] voiceReSpawn;
		public AudioClip[] voiceSpecialItem;
		public AudioClip[] voiceSpotEnemies;
		public AudioClip[] voiceThanks;
		public AudioClip[] voiceHealing;
		public AudioClip[] voiceTakeDown;
		public AudioClip[] voiceStreak;
		public AudioClip[] voiceRevenge;
		public AudioClip[] voiceCritical;
		public AudioClip[] voiceHurts;
		public AudioClip[] voiceHurtByAlly;
	}


	//Animation==========================
	public interface AnimatorParam
	{
		int NameHash { get; set;}
	}

	[System.Serializable]
	public struct AnimatorFloatParam : AnimatorParam
	{

		public static implicit operator int (AnimatorFloatParam param)
		{
			return param.NameHash;
		}

		public int NameHash {
			get {
				return __nameHash;
			}
			set {
				__nameHash = value;
			}
		}
		[SerializeField]
		int __nameHash;

		public float Value;
	}

	[System.Serializable]
	public struct AnimatorBoolParam : AnimatorParam
	{
		public static implicit operator int (AnimatorBoolParam param)
		{
			return param.NameHash;
		}

		public int NameHash {
			get {
				return __nameHash;
			}
			set {
				__nameHash = value;
			}
		}
		[SerializeField]
		int __nameHash;

		public bool Value;
	}

	[System.Serializable]
	public struct AnimatorIntParam : AnimatorParam
	{
		public static implicit operator int (AnimatorIntParam param)
		{
			return param.NameHash;
		}

		public int NameHash {
			get {
				return __nameHash;
			}
			set {
				__nameHash = value;
			}
		}
		[SerializeField]
		int __nameHash;

		public int Value;
	}

	[System.Serializable]
	public class TransformEventTrigger : UnityEvent <Transform>
	{
		
	}

	[System.Serializable]
	public class GameObjectEventTrigger : UnityEvent <GameObject>
	{
		
	}

	[System.Serializable]
	public class CameraEventTrigger : UnityEvent <Camera>
	{

	}

}

namespace UnityStandardAssets.ImageEffects
{
	
}


/// <summary>
/// Environmental common.
/// </summary>
namespace BattleSplash.Environmental
{
	public enum EnvExplosionType
	{
		None,
		Small,
		Normal,
		Big,
	}
}