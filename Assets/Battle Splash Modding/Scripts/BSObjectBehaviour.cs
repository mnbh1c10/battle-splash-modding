﻿using UnityEngine;

using System.Collections;
using System.Collections.Generic;

public class BSObjectBehaviour : MonoBehaviour {

	//Static members
	public static implicit operator GameObject (BSObjectBehaviour b)
	{
		if (b)
			return b.gameObject;
		return null;
	}


	//Caching

	//============================================================================
	//Begin object Pool - static
	//============================================================================
//
//	/// <summary>
//	/// The global gameobject-components dictionary for fast query.
//	/// </summary>
//	private static Dictionary<GameObject,Dictionary<System.Type,List<Component>>> dictComponentsGlobal = new Dictionary<GameObject, Dictionary<System.Type, List<Component>>>();
//
//	/// <summary>
//	/// Store the pool for all the bsobjectbehaviour of a gameobject
//	/// </summary>
//	private static Dictionary<GameObject,BSObjectBehaviour[]> bsObjBehaviourPool = new Dictionary<GameObject, BSObjectBehaviour[]>();
//
//	/// <summary>
//	/// Caching the list of BSObjectBehaviour of a game object.
//	/// </summary>
//	/// <param name="go">GameObject to be queried.</param>
//	public static void BSObjBehaviourCaching (GameObject go)
//	{
//		if (!go)
//			throw new System.NullReferenceException ("No specified object for caching");
////		Debug.Log ("Pre caching " + go.name);
//		if (!bsObjBehaviourPool.ContainsKey (go)) {
//			bsObjBehaviourPool.Add (go, new BSObjectBehaviour[0]);
//		}
//		BSObjectBehaviour [] listBehaviour = go.GetComponents<BSObjectBehaviour> ();
//		if (listBehaviour != null) {
//			bsObjBehaviourPool [go] = listBehaviour;
//		}
//	}
//
//	/// <summary>
//	/// Gets all behaviours from game object.
//	/// </summary>
//	/// <returns>Null if there is no specified GameObject, otherwise the cached behaviours from game object.</returns>
//	/// <param name="go">Game Object to query.</param>
//	/// <param name="queryAnyway">If set to <c>true</c>, when there is no cached behaviour, the method will try to search for any behaviour and attemps to try caching again.</param>
//	public static BSObjectBehaviour[] GetAllBehavioursFromGameObject (Object obj, bool queryAnyway = false)
//	{
//		GameObject go = obj as GameObject;
//
//		if (go == null)
//		{
//			Component c = obj as Component;
//			if (c != null)
//				go = c.gameObject;
//		}
//
//		BSObjectBehaviour[] output = new BSObjectBehaviour[0];
//		if (go)
//		{
//			//No data recorded?
//			if (!bsObjBehaviourPool.TryGetValue(go,out output))
//			{
//				BSObjBehaviourCaching (go);
//				output = bsObjBehaviourPool [go];
//			}
//			else if (queryAnyway) 
//			{
//				//when there is no component found and user insisted to query anyway.
//				if (output.Length <= 0)
//				{
//					BSObjBehaviourCaching (go);
//					if (bsObjBehaviourPool.ContainsKey (go))
//						output = bsObjBehaviourPool [go];
//				}
//			}
//
//		}
//		return output;
//	}
//
	public static T ConverToType<T>(object input)
	{
		return (T)System.Convert.ChangeType (input, typeof(T));
	}
//
//	public static T Cast<T> (object input)
//	{
//		if (input != null)
//		{
//			if (typeof(T).IsAssignableFrom (input.GetType ()))
//			{
//				return (T)input;
//			}
//		}
//		return default(T);
//	}
//		
//	/// <summary>
//	/// Gets the first cached BSObjectBehaviour component from game object.
//	/// </summary>
//	/// <returns>Null if there is no cached behaviour, otherwise the cached behaviour from game object.</returns>
//	/// <param name="go">Game Object to query.</param>
//	/// <param name="queryAnyway">If set to <c>true</c>, when there is no cached behaviour, the method will try to search for the behaviour and attemps to try caching again.</param>
//	public static BSObjectBehaviour GetBehaviourFromGameObject (Object go, bool includeInheritant = true,  bool queryAnyway = false)
//	{
//		BSObjectBehaviour[] output = GetAllBehavioursFromGameObject (go, queryAnyway);
//		if (output.Length > 0)
//			return output [0];
//		return null;
//	}
//
//
//	/// <summary>
//	/// Gets the BSObjectBehaviour from game object.
//	/// </summary>
//	/// <returns>The behaviour from game object.</returns>
//	/// <param name="go">Game Object to query.</param>
//	/// <param name="type">Specific class Type to query (including Inherited class).</param>
//	/// <param name="queryAnyway">If set to <c>true</c>, when there is no cached behaviour, the method will try to search for the behaviour and attemps to try caching again.</param>
//	public static T GetBehaviourFromGameObject<T>(Object go, bool includeInheritant = true,  bool queryAnyway = false)
//	{
//		return Cast<T>(GetBehaviourFromGameObject (go, typeof (T), includeInheritant, queryAnyway));
//	}
//		
//
//	/// <summary>
//	/// Gets the BSObjectBehaviour from game object.
//	/// </summary>
//	/// <returns>The behaviour from game object.</returns>
//	/// <param name="go">Game Object to query.</param>
//	/// <param name="type">Specific class Type to query (excluding Inherited class). Type parameter delivered from BSObjectBehaviour.</param>
//	/// <param name="includeInheritant">If set to <c>true</c>, includes inherited class in query.</param>
//	/// <param name="queryAnyway">If set to <c>true</c>, when there is no cached behaviour, the method will try to search for the behaviour and attemps to try caching again.</param>
//	public static BSObjectBehaviour GetBehaviourFromGameObject (Object go, System.Type type, bool includeInheritant = true, bool queryAnyway = false)
//	{
//		BSObjectBehaviour[] output = GetAllBehavioursFromGameObject (go, queryAnyway);
//
//		for (int i = 0;i<output.Length;i++)
//		{
//			if (output [i])
//			{
//				if (!includeInheritant) {
//					if (output [i].GetType () == type)
//						return output [i];
//				}
//				else
//				{
//					if (type.IsAssignableFrom(output [i].GetType ()))
//						return output [i];
//				}
//			}
//		}
//		return null;
//	}
//

	//============================================================================
	//End object Pool
	//============================================================================


	//============================================================================
	//Other Static methods
	//============================================================================

//	/// <summary>
//	/// Queries for the cached component of this gameobject.<para></para><para></para> 
//	/// If <see cref="allowReAcquire"/>is set to <c>true</c> component will be 
//	/// re-acquired using GetComponent. This is useful in case when you want to query or cache the Component again. 
//	/// This option will result in poor performance if used in every frame.</summary>
//	/// <returns>The component.</returns>
//	/// <param name="gameObject">Game object.</param>
//	/// <param name="inputType">Input type.</param>
//	/// <param name="allowReAcquire">If set to <c>true</c> component will be re-acquired using GetComponent. This is useful 
//	/// in the case when you want to query or cache the Component again. This option will result in poor performance when used frequently.</param>
//	public static Component QueryComponent (GameObject gameObject, System.Type inputType, bool allowReAcquire = false)
//	{
//		if (!gameObject)
//		{
//			try
//			{
//				dictComponentsGlobal.Remove (gameObject);
//			}
//			catch (System.Exception)
//			{
//				
//			}
//			return null;
//		}
//		if (!dictComponentsGlobal.ContainsKey (gameObject)) {
//			CacheComponent (gameObject);
//		}
//		
//		Dictionary<System.Type,List<Component>> dictComponents = dictComponentsGlobal [gameObject];
//		Component component = null;
//		if (dictComponents.ContainsKey(inputType))
//		{
////			if (dictComponents[inputType].Count > 0)
////				component = dictComponents [inputType];
//			component = GetFirstComponentFromDict (gameObject, inputType);
//			
//			if (component == null && allowReAcquire) 
//			{
//				CacheComponent (gameObject);
////				component = gameObject.GetComponent (inputType);
//				component = GetFirstComponentFromDict (gameObject, inputType);
//			}
//		}
//		else
//		{
//			if (allowReAcquire) {
//				CacheComponent (gameObject);
//				component = GetFirstComponentFromDict (gameObject, inputType);
//			}
//		}
////		dictComponents [inputType].Add (component);
//		return component;
//	}
//
//	private static Component GetFirstComponentFromDict (GameObject gameObject, System.Type inputType)
//	{
//		if (gameObject)
//		{
//			if (dictComponentsGlobal.ContainsKey (gameObject))
//			{
//				Dictionary<System.Type, List<Component>> dictComponent = dictComponentsGlobal [gameObject];
//				if (dictComponent.ContainsKey (inputType))
//				{
//					if (dictComponent [inputType] != null)
//					{
//						if (dictComponent [inputType].Count > 0)
//						{
//							return dictComponent[inputType] [0];
//						}
//					}
//				}
//			}
//		}
//		return null;
//	}
//
//	/// <summary>
//	/// Manually Cache all components of a GameObject. Noted that this will be slow.
//	/// </summary>
//	/// <param name="gameObject">Game object.</param>
//	public static void CacheComponent (GameObject gObject)
//	{
//		if (gObject)
//		{
//			if (!dictComponentsGlobal.ContainsKey (gObject))
//				dictComponentsGlobal.Add (gObject, new Dictionary<System.Type, List<Component>> ());
//
//			Dictionary<System.Type,List<Component>> dictComponents = dictComponentsGlobal [gObject];
//			Component[] components = gObject.GetComponents<Component> ();
//			dictComponents.Clear ();
//
//			bool hasBSOBehaviour = false;
//			//Old implementation, needs to be updated
//			 
//			for (int i = 0;i<components.Length;i++)
//			{
//				if (!dictComponents.ContainsKey (components [i].GetType ()))
//					dictComponents.Add (components [i].GetType (), new List<Component> ());
//				dictComponents [components [i].GetType ()].Add (components [i]);
//				if (typeof (BSObjectBehaviour).IsAssignableFrom (components[i].GetType ()))
//				{
//					hasBSOBehaviour = true;
//				}
//			}
//
////			if (hasBSOBehaviour && !bsObjBehaviourPool.ContainsKey (gObject))
////			{
////				BSObjBehaviourCaching (gObject);
////			}
//			//New, possible slower than the old one: Iterate all possible basetype of a class
////			for (int i = 0;i<components.Length;i++)
////			{
////				Component component = components [i];
////				System.Type currentType = components [i].GetType ();
////				while (currentType != typeof (UnityEngine.Object) || currentType != typeof (Component) || currentType != typeof (MonoBehaviour)) {
////					if (currentType == null)
////						break;
////					if (!dictComponents.ContainsKey (currentType))
////						dictComponents.Add (currentType, new List<Component> ());
////					dictComponents [currentType].Add (component);
////					currentType = currentType.BaseType;
////				}
////			}
//		}
//		//CleanUp ();
//	}

//	public static void ReleaseCache(GameObject go)
//	{
//		bsObjBehaviourPool.Remove (go);
//
//		dictComponentsGlobal.Remove (go);
//	}

//	/// <summary>
//	/// Queries for the cached component of this gameobject.
//	/// </summary>
//	/// <returns>The component.</returns>
//	/// <param name="go">Go.</param>
//	/// <typeparam name="T">The 1st type parameter.</typeparam>
//	public static T QueryComponent <T> (GameObject go) where T: Component
//	{
//		return (T)QueryComponent (go, typeof(T));
//	}
//
//	/// <summary>
//	/// Queries for the cached component of this gameobject.
//	/// </summary>
//	/// <returns>The component.</returns>
//	/// <param name="component">Component.</param>
//	/// <typeparam name="T">The 1st type parameter.</typeparam>
//	public static T QueryComponent <T> (Component component) where T: Component
//	{
//		if (component)
//			return QueryComponent<T> (component.gameObject);
//		return null;
//	}

//	public static void ReleaseAllCaches ()
//	{
//		dictComponentsGlobal.Clear ();
//		bsObjBehaviourPool.Clear ();
//		//count = 0;
//		for (int i = 0;i<dictComponentsGlobal.Keys.Count;i++)
//		{
//			if (!dictComponentsGlobal.Keys [i]) {
//				dictComponentsGlobal.Remove (dictComponentsGlobal.Keys [i]);
//				//count++;
//			}
//
//		}
//		//Debug.Log (string.Format ("Clean up {0} cached gameobjects.", count));
//
//		//count = 0;
//		for (int i = 0;i<bsObjBehaviourPool.Keys.Count;i++)
//		{
//			if (!bsObjBehaviourPool.Keys [i]) {
//				bsObjBehaviourPool.Remove (bsObjBehaviourPool.Keys [i]);
//				//count++;
//			}
//	
//		}
//		System.GC.Collect ();
//	}

//	public static IEnumerator CleanUpCoroutine (float delay = 0)
//	{
//		
//		//count = 0;
//		for (int i = 0;i<dictComponentsGlobal.Keys.Count;i++)
//		{
//			if (!dictComponentsGlobal.Keys [i]) {
//				dictComponentsGlobal.Remove (dictComponentsGlobal.Keys [i]);
//				//count++;
//			}
//			if (delay > 0)
//				yield return new WaitForSeconds (delay);
//			else
//				yield return new WaitForEndOfFrame ();
//		}
//		//Debug.Log (string.Format ("Clean up {0} cached gameobjects.", count));
//
//		//count = 0;
//		for (int i = 0;i<bsObjBehaviourPool.Keys.Count;i++)
//		{
//			if (!bsObjBehaviourPool.Keys [i]) {
//				bsObjBehaviourPool.Remove (bsObjBehaviourPool.Keys [i]);
//				//count++;
//			}
//			if (delay > 0)
//				yield return new WaitForSeconds (delay);
//			else
//				yield return new WaitForEndOfFrame ();
//		}
//
//		//Debug.Log (string.Format ("Clean up {0} cached gameobjects Behaviour.", count));
//	}

	//============================================================================
	//End Static
	//============================================================================

	/// <summary>
	/// Gets or sets the persistant name of the object. This is to avoid GameObject.name get modified by the engine.
	/// </summary>
	/// <value>The name of the object if any, otherwise the default GameObject.name.</value>
	public string ObjectName
	{
		get
		{
			if (string.IsNullOrEmpty (_objectName))
				return name;
			return _objectName;
		}
		set
		{
			_objectName = value;
		}
	}


	public virtual Collider ColliderComponent
	{
		get
		{
			if (!_collider) {
				_collider = null;
				if (IsRecachable)
					_collider = GetComponent<Collider> ();
			}
			return _collider;
		}
	}

	public virtual Rigidbody RigidbodyComponent
	{
		get
		{
			if (!_rigidbody) {
				_rigidbody = null;
				if (IsRecachable)
					_rigidbody = GetComponent<Rigidbody> ();
			}
			return _rigidbody;
		}
	}

	public virtual Renderer RendererComponent
	{
		get
		{
			if (!_renderer) {
				_renderer = null;
				if (IsRecachable)
					_renderer = GetComponent<Renderer> ();
			}
			return _renderer;
		}
	}

	public virtual MeshRenderer MeshRendererComponent
	{
		get
		{
			if (!_meshRenderer) {
				_meshRenderer = null;
				if (IsRecachable)
					_meshRenderer = GetComponent<MeshRenderer> ();
			}
			return _meshRenderer;
		}
	}

	public virtual MeshFilter MeshFilterComponent
	{
		get
		{
			if (!_meshFilter) {
				_meshFilter = null;
				if (IsRecachable)
					_meshFilter = GetComponent<MeshFilter> ();
			}
			return _meshFilter;
		}
	}

	public virtual SkinnedMeshRenderer SkinnedMeshRendererComponent
	{
		get
		{
			if (!_skinnedMeshRenderer) {
				_skinnedMeshRenderer = null;
				if (IsRecachable)
					_skinnedMeshRenderer = GetComponent<SkinnedMeshRenderer> ();
			}
			return _skinnedMeshRenderer;
		}
	}

	public virtual Animator AnimatorComponent
	{
		get
		{
			if (!_animator) {
				_animator = null;
				if (IsRecachable)
					_animator = GetComponent<Animator> ();
			}
			return _animator;
		}
	}

	public virtual NetworkView NetworkViewComponent
	{
		get
		{
			if (!_networkView) {
				_networkView = null;
				if (IsRecachable)
					_networkView = GetComponent<NetworkView> ();
			}
			return _networkView;
		}
	}

	public virtual ParticleSystem ParticleSystemComponent
	{
		get
		{
			if (!_particleSystem) {
				_particleSystem = null;
				if (IsRecachable)
					_particleSystem = GetComponent<ParticleSystem> ();
			}
			return _particleSystem;
		}
	}

	public virtual Camera CameraComponent
	{
		get
		{
			if (!_camera) {
				_camera = null;
				if (IsRecachable)
					_camera = GetComponent<Camera> ();
			}
			return _camera;
		}
	}

	public virtual LODGroup LODGroupComponent
	{
		get
		{
			if (!_LODGroup) {
				_LODGroup = null;
				if (IsRecachable)
					_LODGroup = GetComponent<LODGroup> ();
			}
			return _LODGroup;
		}
	}

	public virtual AudioSource AudioSourceComponent
	{
		get
		{
			if (!_audioSource) {
				_audioSource = null;
				if (IsRecachable)
					_audioSource = GetComponent<AudioSource> ();
			}
			return _audioSource;
		}
	}

	public enum ComponentCacheMode
	{
		[Tooltip ("Only cache component during Editor design mode.")]
		DesignOnly,
		[Tooltip ("Allow recaching missing component during runtime (slower).")]
		RunTime,
	}

	[Header ("Cached Component")]

	[Tooltip ("Determine whether the component should recache missing component.\n\nSetting it to \"Run Time\" might affect performance, yet it always get the correct result.\n\n" +
		"However, it is possible to change this value to \"Run Time\" in scripting during runtime, get the desired cached component, then switch it back to \"Design Only\" for one-time accurate result.")]
	/// <summary>
	/// Determine whether the component should recache missing component. 
	/// Setting it to "Run Time" might affect performance, yet it always get the correct result. 
	/// However, it is possible to change this value to "Run Time" in scripting during runtime, 
	/// get the desired cached component, then switch it back to "Design Only" for one-time accurate result.
	/// </summary>
	public ComponentCacheMode componentCacheMode = ComponentCacheMode.RunTime;

	private bool IsRecachable
	{
		get
		{
			if (Application.isPlaying)
				return componentCacheMode == ComponentCacheMode.RunTime;
			return true;
		}
	}

	[SerializeField]
	private Collider _collider = null;
	[SerializeField]
	private Rigidbody _rigidbody = null;
	[SerializeField]
	private Renderer _renderer = null;
	[SerializeField]
	private MeshRenderer _meshRenderer = null;
	[SerializeField]
	private MeshFilter _meshFilter = null;
	[SerializeField]
	private SkinnedMeshRenderer _skinnedMeshRenderer = null;
	[SerializeField]
	private Animator _animator = null;
	[SerializeField]
	private NetworkView _networkView = null;
	[SerializeField]
	private ParticleSystem _particleSystem = null;
	[SerializeField]
	private Camera _camera = null;
	[SerializeField]
	private LODGroup _LODGroup = null;
	[SerializeField]
	private AudioSource _audioSource = null;

	[Header ("Instance Properties")]
	[SerializeField]
	/// <summary>
	/// The persistant name of the object. This is to avoid GameObject.name get modified by the engine.
	/// </summary>
	[Tooltip ("The persistant name of the object. This is to avoid GameObject.name get modified by the engine.")]
	private string _objectName = "";

//	private Dictionary<System.Type,Component> dictComponents = new Dictionary<System.Type, Component>();
//	/// <summary>
//	/// Get component attached to this gameobject.
//	/// <para></para><para></para>
//	/// If the component is found and not cached, it will be cached for fast
//	/// addressing. If the component is not found, it won't be cached even if it get added later because of performance. 
//	/// To recaching, use <see cref="QueryComponent"/> static method instead.
//	/// See <see cref="BSObjectBehaviour.QueryComponent"/>for more info
//	/// </summary>
//	/// <returns>The component.</returns>
//	/// <param name="inputType">Input type.</param>
//	public Component AcquireComponent(System.Type inputType)
//	{
//		return QueryComponent (gameObject, inputType);
//	}
//
//	/// <summary>
//	/// Get component attached to this gameobject.
//	/// <para></para><para></para>
//	/// If the component is found and not cached, it will be cached for fast
//	/// addressing. If the component is not found, it won't be cached even if it get added later because of performance. 
//	/// To recaching, use <see cref="QueryComponent"/> static method instead.
//	/// <para></para><para></para>
//	/// See <see cref="BSObjectBehaviour.QueryComponent"/>for more info
//	/// </summary>
//	/// <returns>The component.</returns>
//	/// <typeparam name="T">Input type.</typeparam>
//	public T AcquireComponent<T>() where T: Component
//	{
//		return (T)AcquireComponent (typeof(T));
//	}
//
	protected virtual void OnDestroy ()
	{

//		//if (bsObjBehaviourPool.ContainsKey (gameObject))
//		bsObjBehaviourPool.Remove (gameObject);
//		
//		//if (dictComponentsGlobal.ContainsKey (gameObject))
//		dictComponentsGlobal.Remove (gameObject);
////
//		Transform[] allChild = gameObject.GetComponentsInChildren<Transform> (true);
//		for (int i = 0; i < allChild.Length; i++)
//		{
//			//if (bsObjBehaviourPool.ContainsKey (allChild[i].gameObject))
//			bsObjBehaviourPool.Remove (allChild[i].gameObject);
//
//			//if (dictComponentsGlobal.ContainsKey (allChild[i].gameObject))
//			dictComponentsGlobal.Remove (allChild[i].gameObject);
//		}

	}

	//===================Monoes====================

	/// <summary>
	/// Recache all missing component (slow).
	/// </summary>
	public virtual void ReCache ()
	{
		if (!_collider)
			_collider = GetComponent <Collider> ();
		if (!_rigidbody)
			_rigidbody = GetComponent <Rigidbody> ();
		if (!_renderer)
			_renderer = GetComponent <Renderer> ();
		if (!_meshRenderer)
			_meshRenderer = GetComponent <MeshRenderer> ();
		if (!_meshFilter)
			_meshFilter = GetComponent <MeshFilter> ();
		if (!_skinnedMeshRenderer)
			_skinnedMeshRenderer = GetComponent <SkinnedMeshRenderer> ();
		if (!_animator)
			_animator = GetComponent <Animator> ();
		if (!_networkView)
			_networkView = GetComponent <NetworkView> ();
		if (!_particleSystem)
			_particleSystem = GetComponent <ParticleSystem> ();
		if (!_camera)
			_camera = GetComponent <Camera> ();
		if (!_LODGroup)
			_LODGroup = GetComponent <LODGroup> ();
		if (!_audioSource)
			_audioSource = GetComponent<AudioSource> ();
	}

	[ContextMenu ("Validate")]
	protected virtual void OnValidate ()
	{
		if (!Application.isPlaying) {
			ReCache ();
		}
	}

}
