﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

using CustomAttributes;

public class AnimatorEventReceiver : MonoBehaviour {

	protected struct FastMethodInfo
	{
		public readonly MethodInfo method;
		public readonly int paramCount;

		public FastMethodInfo (MethodInfo method, int paramCount)
		{
			this.method = method;
			this.paramCount = paramCount;
		}
	}

	public virtual Component receiver
	{
		get
		{
			if (!_receiver)
			{
				_receiver = null;
				onAnimatorIKMethod = null;
				dictMethod.Clear ();
			}
			return _receiver;
		}
		set
		{
			dictMethod.Clear ();
			_receiver = value;
			if (value)
			{
				MonoBehaviour monoBehaviour = value as MonoBehaviour;
				if (monoBehaviour)
				{
					MethodInfo[] allMethods = monoBehaviour.GetType ().GetMethods (BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
					for (int i = 0; i < allMethods.Length; i++)
					{
						MethodInfo method = allMethods [i];
						if (method.GetCustomAttributes (typeof(CallableMethod), true).Length > 0)
						{
							FastMethodInfo fm = new FastMethodInfo (method, method.GetParameters ().Length);
							dictMethod.Add (method.Name, fm);
							if (method.Name.Equals ("OnAnimatorIK") && fm.paramCount == 1)
							{
								onAnimatorIKMethod = method;
							}
						}
					}
				}
			}
		}
	}
	private Component _receiver;
	protected Dictionary <string,FastMethodInfo> dictMethod = new Dictionary<string, FastMethodInfo> ();

	protected MethodInfo onAnimatorIKMethod = null;

	public delegate void OnAnimatorIKCustom (int index);
	[System.Serializable]
	public class OnAnimatorIKCustomUEvent : UnityEvent <int>
	{
		
	}
	/// <summary>
	/// On Animator IK for serialization in Unity (this should be used sparingly)
	/// </summary>
	[Tooltip ("On Animator IK for serialization in Unity (this should be used sparingly)")]
	public OnAnimatorIKCustomUEvent onAnimatorIKCustomUEvent = new OnAnimatorIKCustomUEvent ();
	/// <summary>
	/// On Animator IK. (This should be used for performance)
	/// </summary>
	public event OnAnimatorIKCustom onAnimatorIKCustom = null;

	protected void SendMethod (string name)
	{
		SendMethod (name, SendMessageOptions.RequireReceiver, null);
	}

	protected void SendMethod (string name, SendMessageOptions sendOption)
	{
		SendMethod (name, sendOption, null);
	}

	protected void SendMethod (string name, object value)
	{
		SendMethod (name, SendMessageOptions.RequireReceiver, value);
	}

	void SendMethod (string name, SendMessageOptions sendOption, object value)
	{
		if (receiver) {
			FastMethodInfo fm;
			if (dictMethod.TryGetValue (name, out fm)) {
				if (fm.paramCount > 0)
					fm.method.Invoke (receiver, new object [] { value });
				else
					fm.method.Invoke (receiver, null);
			} else {
				receiver.SendMessage (name, value, sendOption);
			}
		}
	}

	void OnAnimatorIK(int layerIndex)
	{
		if (receiver) {
//			SendMethod ("OnAnimatorIK", layerIndex);
			if (onAnimatorIKMethod != null)
				onAnimatorIKMethod.Invoke (receiver, new object [] { layerIndex });
//			else
//				SendMethod ("OnAnimatorIK", layerIndex);
		}
		else
		{
			onAnimatorIKMethod = null;
		}
		if (onAnimatorIKCustom != null) {
//			Debug.Log (name +" is Invoking OnAnimatorIK " + layerIndex.ToString (), this);
			onAnimatorIKCustom.Invoke (layerIndex);
		}
		onAnimatorIKCustomUEvent.Invoke (layerIndex);
	}

	void CustomEvent (string methodName)
	{
		if (receiver)
			SendMethod (methodName);
	}

	void CustomEvent (AnimationEvent animEvt)
	{
		if (receiver)
			SendMethod (animEvt.stringParameter.Split(' ')[0], SendMessageOptions.DontRequireReceiver);
	}

	protected virtual void PlayRunSound (float volumeScale)
	{
		if (receiver) 
			SendMethod ("PlayRunSound", volumeScale);
	}

	protected virtual void PlayWalkSound (float volumeScale)
	{
		if (receiver)
			SendMethod ("PlayWalkSound", volumeScale);
	}

	protected virtual void PlayWeaponSoundBolt (float volumeScale)
	{
		if (receiver)
			SendMethod ("PlayWeaponSoundBolt", volumeScale);
	}

	protected virtual void PlayWeaponSoundReload (float volumeScale)
	{
		if (receiver)
			SendMethod ("PlayWeaponSoundReload", volumeScale);
	}

	protected virtual void PlayWeaponSoundClipIn (float volumeScale)
	{
		if (receiver)
			SendMethod ("PlayWeaponSoundClipIn", volumeScale);
	}

	protected virtual void PlayWeaponSoundClipOut (float volumeScale)
	{
		if (receiver)
			SendMethod ("PlayWeaponSoundClipOut", volumeScale);
	}

	protected virtual void PlayWeaponSoundBoltBack (float volumeScale)
	{
		if (receiver)
			SendMethod ("PlayWeaponSoundBoltBack", volumeScale);
	}

	void OnDestroy ()
	{
		onAnimatorIKMethod = null;
	}

	void OnValidate ()
	{
		if (receiver)
		{
			
		}
	}

}
