﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace CustomAttributes
{

	public enum Visibility
	{
		Off,
		DebugOnly,
		Visible,
	}

	[System.AttributeUsage(System.AttributeTargets.Field | System.AttributeTargets.Property)]
	public class FieldMetaData : System.Attribute
	{
		/// <summary>
		/// Gets the public default name of the field.
		/// </summary>
		/// <value>The name which is shown to other.</value>
		public string DefaultPublicName
		{
			get
			{
				return defaultPublicName;
			}
		}
		private string defaultPublicName = "";

		/// <summary>
		/// Should the field be visible?
		/// </summary>
		/// <value><c>true</c> if visible; otherwise, <c>false</c>.</value>
		public Visibility Visible
		{
			get
			{
				return isShown;
			}
		}
		private Visibility isShown = Visibility.Visible;

		public string Tag
		{
			get
			{
				return fieldTag;
			}
		}
		private string fieldTag = "";

//		public FieldMetaData(string name) : this ("", name, Visibility.Visible)
//		{
//			
//		}

		public FieldMetaData(string name, Visibility visible = Visibility.Visible) : this ("", name, visible)
		{
			
		}

		public FieldMetaData(string tag, string name, Visibility visible = Visibility.Visible)
		{
			fieldTag = tag;
			defaultPublicName = name;
			isShown = visible;
		}
	}
	/// <summary>
	/// Attribute for hiding any field with given name.
	/// </summary>
	[System.AttributeUsage(System.AttributeTargets.Class)]
	public class HideFieldsName : System.Attribute
	{
		public string[] HidedFields
		{
			get
			{
				return listHidedField.ToArray ();
			}
		}
		private List<string> listHidedField = new List<string>();

		public bool IsHidden (string fieldName)
		{
			return listHidedField.Contains (fieldName);
		}

		public HideFieldsName (params string[] hiddingFields) 
		{
			listHidedField.AddRange (hiddingFields);
		}
	}

	public class EnumFlagsAttribute : PropertyAttribute
	{
		public EnumFlagsAttribute() { }
	}

	#if UNITY_EDITOR
	[UnityEditor.CustomPropertyDrawer(typeof(EnumFlagsAttribute))]
	public class EnumFlagsAttributeDrawer : UnityEditor.PropertyDrawer
	{
		public override void OnGUI(Rect _position, UnityEditor.SerializedProperty _property, GUIContent _label)
		{
			// Change check is needed to prevent values being overwritten during multiple-selection
			UnityEditor.EditorGUI.BeginChangeCheck ();
			int newValue = UnityEditor.EditorGUI.MaskField( _position, _label, _property.intValue, _property.enumNames );
			if (UnityEditor.EditorGUI.EndChangeCheck ()) {
				_property.intValue = newValue;
			}
		}
	}
	#endif


	[System.AttributeUsage(System.AttributeTargets.Field)]
	public class CharacterDescription : System.Attribute
	{
		public bool IsDescription
		{
			get
			{
				return isDescription;
			}
		}

		bool isDescription = false;

		public CharacterDescription () : this (true)
		{
			
		}

		public CharacterDescription (bool desValue)
		{
			isDescription = desValue;
		}
	}

	/// <summary>
	/// Attribute to Disable field from being edited in the editor.
	/// </summary>
	[System.AttributeUsage (System.AttributeTargets.Field)]
	public class DisabledFieldAttribute : PropertyAttribute
	{
		public DisabledFieldAttribute ()
		{
			
		}
	}

	/// <summary>
	/// Attribute to expose method to the outside caller, replacing SendMessage
	/// </summary>
	[System.AttributeUsage (System.AttributeTargets.Method)]
	public class CallableMethod : System.Attribute
	{
		public CallableMethod ()
		{
			
		}
	}

	/// <summary>
	/// Attribute to expose editor tag field.
	/// </summary>
	[System.AttributeUsage (System.AttributeTargets.Field)]
	public class EditorTagField : PropertyAttribute
	{
		public EditorTagField ()
		{
			
		}
	}
}