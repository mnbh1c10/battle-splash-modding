﻿using UnityEngine;
using System.Collections;

using BattleSplash.Common;

public class EquipmentRig : MonoBehaviour {

	public Animator characterAnimator
	{
		get
		{
			return _characterAnimator;
		}
		set
		{
			_characterAnimator = value;
			if (value)
			{
				Transform c_chest = value.GetBoneTransform (HumanBodyBones.Chest);
				if (chest && c_chest) {
					chest.SetParent (c_chest, true);
					chest.localPosition = Vector3.zero;
				}
				Transform c_hips = value.GetBoneTransform (HumanBodyBones.Hips);
				if (hips && c_hips) {
					hips.SetParent (c_hips, true);
					hips.localPosition = Vector3.zero;
				}
			}
			else
			{
				if (hips)
					hips.SetParent (null);
				if (chest)
					chest.SetParent (null);
			}
		}
	}

	public Transform chest;
	public Transform hips;
	public Transform weapon;
	public Transform sword;
	public Transform armShield;
	public Transform rightGun;
	public Transform leftGun;
	public Transform grenade;
	public Transform mag;

	private Animator _characterAnimator;

	void Awake ()
	{
		Renderer[] allRenderers = GetComponentsInChildren<Renderer> (true);
		for (int i = 0;i<allRenderers.Length;i++)
		{
			allRenderers [i].enabled = false;
		}
	}

	public Transform GetEquipmentTransform (EquipmentPosition position)
	{
		switch (position)
		{
		case EquipmentPosition.Chest:
			return chest;
//			break;
		case EquipmentPosition.Hips:
			return hips;
//			break;
		case EquipmentPosition.LongWeapon:
			return weapon;
//			break;
		case EquipmentPosition.Sword:
			return sword;
//			break;
		case EquipmentPosition.ArmShield:
			return armShield;
//			break;
		case EquipmentPosition.LeftGun:
			return leftGun;
//			break;
		case EquipmentPosition.RightGun:
			return rightGun;
//			break;
		case EquipmentPosition.Grenade:
			return grenade;
//			break;
		case EquipmentPosition.Magazine:
			return mag;
//			break;
		}
		return null;
	}
}
