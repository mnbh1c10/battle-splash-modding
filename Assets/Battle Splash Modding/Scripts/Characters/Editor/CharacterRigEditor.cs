﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Text;

[CustomEditor (typeof (CharacterRig))]
public class CharacterRigEditor : Editor {

	public override void OnInspectorGUI ()
	{
		CharacterRig charRig = (CharacterRig)target;

        if (GUILayout.Button ("Auto map references"))
        {
            charRig.ReCache();
            charRig.Validate();
        }

        if (!charRig.AnimatorComponent.avatar)
        {
            EditorGUILayout.HelpBox("Error: No humanoid avatar specified for animator.", MessageType.Error);
        }
        else if (!charRig.AnimatorComponent.avatar.isHuman)
        {
            EditorGUILayout.HelpBox("Error: Animator Avatar MUST be Humanoid.", MessageType.Error);
        }

		if (!charRig.overridedHandL)
		{
			EditorGUILayout.HelpBox("Error: Rig Missing 'Overrided Hand L'.", MessageType.Error);
		}

		if (!charRig.overridedHandR)
		{

			EditorGUILayout.HelpBox("Error: Rig Missing 'Overrided Hand R'.", MessageType.Error);
		}

		if (!charRig._FPSRigPrefab)
		{
			EditorGUILayout.HelpBox("Warning: No FPS Rig detected, FPS supports will be discarded.", MessageType.Warning);
		}

		if (!charRig.AnimatorComponent.runtimeAnimatorController)
		{
			EditorGUILayout.HelpBox("Warning: No Animator Controller Detected, Default controller will be used.", MessageType.Warning);
		}

		base.OnInspectorGUI();
	}

	float handleSize = 1f;

	void DrawTransformHandles (Transform t)
	{
        Transform transform = t;
        float size = handleSize * HandleUtility.GetHandleSize (t.position);

        Color col = Handles.color;

        Handles.color = Handles.xAxisColor;
        Handles.ArrowHandleCap(
            0,
            transform.position,
            transform.rotation * Quaternion.LookRotation(Vector3.right),
            size,
            EventType.Repaint
        );
        Handles.color = Handles.yAxisColor;
        Handles.ArrowHandleCap(
            0,
            transform.position,
            transform.rotation * Quaternion.LookRotation(Vector3.up),
            size,
            EventType.Repaint
        );
        Handles.color = Handles.zAxisColor;
        Handles.ArrowHandleCap(
            0,
            transform.position,
            transform.rotation * Quaternion.LookRotation(Vector3.forward),
            size,
            EventType.Repaint
        );
        Handles.color = Color.white;
        Handles.CubeHandleCap(0, transform.position, transform.rotation, size / 10f, EventType.Repaint);

        Handles.color = col;
    }

	protected virtual void OnSceneGUI ()
	{
        if (Event.current.type == EventType.Repaint)
        {
            CharacterRig charRig = (CharacterRig)target;
            if (charRig.overridedHandL)
                DrawTransformHandles(charRig.overridedHandL);
            else
            {
                Animator anim = charRig.AnimatorComponent;
                if (anim)
                {
                    Transform hand = anim.GetBoneTransform(HumanBodyBones.LeftHand);
                    if (hand)
                    {
                        DrawTransformHandles(hand);
                    }
                }
            }
            if (charRig.overridedHandR)
                DrawTransformHandles(charRig.overridedHandR);
            else
            {
                Animator anim = charRig.AnimatorComponent;
                if (anim)
                {
                    Transform hand = anim.GetBoneTransform(HumanBodyBones.RightHand);
                    if (hand)
                    {
                        DrawTransformHandles(hand);
                    }
                }
            }
        }
    }
}
