﻿using UnityEngine;
using System.Collections;

namespace BattleSplash.CullingUtils
{
	[DisallowMultipleComponent]
	public sealed class ObjectVisibilityChecker : MonoBehaviour
	{

		[Tooltip ("Renderers to determined the visibility of the rig. This is useful for some special effects that only available when character is in view.")]
		[SerializeField]
		Renderer[] visibilityRenderers = new Renderer[] {};
		int lastUpdatedFrameTime = -1;

		public bool IsVisible
		{
			get
			{
				if (lastUpdatedFrameTime < Time.frameCount) {
					lastUpdatedFrameTime = Time.frameCount;
					bool hasRenderer = false;
					for (int i = 0; i < visibilityRenderers.Length; i++) {
						if (visibilityRenderers [i]) {
							hasRenderer = true;
							if (visibilityRenderers [i].isVisible) {
								_isVisible = true;
								break;
							}
						} else {
							//Release the reference if that renderer was destroyed during gameplay
							visibilityRenderers [i] = null;
						}
					}
					if (!hasRenderer)
						_isVisible = IsVisibleOn ();
				}
				return _isVisible;
			}
		}
		private bool _isVisible;

		/// <summary>
		/// Determines whether this instance is visible on the specified camera. (null for every camera)
		/// </summary>
		/// <returns><c>true</c> if this instance is visible on the specified cam; otherwise, <c>false</c>.</returns>
		/// <param name="cam">Cam.</param>
		bool IsVisibleOn (Camera cam = null)
		{
			Plane[] camPlanes = null;
			if (cam)
			{
				camPlanes = GeometryUtility.CalculateFrustumPlanes (cam);
				return GeometryUtility.TestPlanesAABB (camPlanes, new Bounds (transform.position, Vector3.one));
			}
			else
			{
				if (Camera.allCamerasCount > 0) {
					Camera[] allCam = Camera.allCameras;
					for (int i = 0; i < allCam.Length; i++) {
						Camera c = allCam [i];
						camPlanes = GeometryUtility.CalculateFrustumPlanes (c);
						if (GeometryUtility.TestPlanesAABB (camPlanes, new Bounds (transform.position, Vector3.one)))
							return true;
					}
				}
			}
			return false;
		}

	}

}