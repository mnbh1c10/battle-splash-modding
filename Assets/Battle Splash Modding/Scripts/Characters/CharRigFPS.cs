﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using BattleSplash.Modding;

[RequireComponent (typeof (Animator))]
[RequireComponent(typeof (AnimatorEventReceiver))]
public sealed class CharRigFPS : ModdingBase {
	
	public int fpsLayer;
	public Transform cameraCenterStable;
	public Transform cameraCenterImmerse;
	public Transform overridedHandL;
	public Transform overridedHandR;

	protected override void OnValidate ()
	{
		base.OnValidate ();
		fpsLayer = Mathf.Clamp (fpsLayer, 0, 31);
	}
}
