﻿using UnityEngine;
using System.Collections;

using BattleSplash.Common;

using BattleSplash.Modding;
using BattleSplash.CullingUtils;

[RequireComponent(typeof (Animator))]
[RequireComponent (typeof (ObjectVisibilityChecker))]
[RequireComponent(typeof (AnimatorEventReceiver))]
public sealed class CharacterRig : ModdingBase {
	public string characterName = "";
	public HeadControlMode headControlMode = HeadControlMode.AnimatorLayer;

	public Transform cameraCenter;
	public Transform cameraCenterFPS;
	public Transform overridedHead;
	public Transform overridedChest;
	public Transform overridedHips;
	public Transform overridedHandL;
	public Transform overridedHandR;

	public EquipmentRig _equipmentRigPrefab;

	public CharRigFPS _FPSRigPrefab;
	public CharRigFPS FPSRig
	{
		get
		{
			if (!_FPSRig && _FPSRigPrefab)
			{
				if (_FPSRigPrefab)
				{
					if (!IsSameRoot (transform, _FPSRigPrefab.transform))
					{
						_FPSRig = (Instantiate (_FPSRigPrefab.gameObject, transform) as GameObject).GetComponent<CharRigFPS> ();
						_FPSRig.transform.localPosition = Vector3.zero;
						_FPSRig.transform.localRotation = Quaternion.identity;
					}
					else
					{
						_FPSRig = _FPSRigPrefab;
					}
				}
			}
			return _FPSRig;
		}
	}
	private CharRigFPS _FPSRig;

	public EquipmentRig equipmentRig
	{
		get
		{
			
			return _equipmentRig;
		}
	}
	private EquipmentRig _equipmentRig;

	public ObjectVisibilityChecker visibilityChecker
	{
		get
		{
			if (!_visibilityChecker)
				_visibilityChecker = GetComponent <ObjectVisibilityChecker> ();
			return _visibilityChecker;
		}
	}
	[SerializeField]
	private ObjectVisibilityChecker _visibilityChecker;

	bool IsSameRoot (Transform t1, Transform t2)
	{
		if (t1 == null || t2 == null)
			return false;
		return t1.root == t2.root && t1.root;
	}

	public bool IsVisible
	{
		get
		{
			return visibilityChecker.IsVisible;
		}
	}

	void Awake ()
	{
		
		if (!_equipmentRig && _equipmentRigPrefab)
		{
			if (_equipmentRigPrefab) {
				_equipmentRig = (Instantiate (_equipmentRigPrefab.gameObject, transform) as GameObject).GetComponent<EquipmentRig> ();
				_equipmentRig.transform.localPosition = Vector3.zero;
				_equipmentRig.transform.localRotation = Quaternion.identity;
				_equipmentRig.characterAnimator = AnimatorComponent;
			}
		}
	}

	protected override void OnDestroy ()
	{
		if (FPSRig != _FPSRigPrefab && FPSRig)
			Destroy (FPSRig.gameObject);
		if (_equipmentRig)
		{
			Destroy (_equipmentRig);
		}
	}

	void OnValidate ()
	{
		if (visibilityChecker)
		{

		}
	}

	public void Validate ()
	{
		if (Application.isPlaying)
		{
			if (overridedHandL)
			{
				overridedHandL.name = "item_L";
			}
			if (overridedHandR)
			{
				overridedHandR.name = "item_R";
			}
		}

		if (AnimatorComponent)
		{
			if (!overridedHead)
				overridedHead = AnimatorComponent.GetBoneTransform(HumanBodyBones.Head);
			if (!overridedChest)
				overridedChest = AnimatorComponent.GetBoneTransform(HumanBodyBones.Chest);
			if (!overridedHips)
				overridedHips = AnimatorComponent.GetBoneTransform(HumanBodyBones.Hips);
		}
	}
}
