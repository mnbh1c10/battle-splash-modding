﻿using UnityEngine;
using UnityEngine.Profiling;
using UnityEngine.UI;
using UnityEngine.EventSystems;

using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Reflection;
using System.Security.Cryptography;

using System.Linq.Expressions;

using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;

using CustomAttributes;
//
using BattleSplash.Common;
//using BattleSplash.Weapons;
//using BattleSplash.Languages;

public static partial class Utils {

	#region Enums and Helpers
	//Enums and helpers=======================
	public enum BoundPosition
	{
		UPPER_CENTER,
		LOWER_CENTER,
	}
		
	public interface IStringPath
	{
		char separator { get; set;}
		string path { get;}
		string localPath { get;}
		int pathCount {get;}
		int localPathCount {get;}

		Object GetObject (Transform travelRoot);
		void GetPath (Object transform, Transform root = null);
		void FromString (string path, char sep = ':');
		int[] ToArray ();
	}

	[System.Serializable]
	public struct TransformIndexPath : IStringPath
	{

		public static implicit operator string (TransformIndexPath tiPath)
		{
			return tiPath.path;
		}



		public char separator {
			get
			{
				return _sep;
			}
			set
			{
				_sep = value;
			}
		}

		[SerializeField]
		[HideInInspector]
		private char _sep;

		public string path {
			get {
				return Utils.ArrayToString (separator.ToString (), "", "", "", "", System.Array.ConvertAll (ToArray (), item => (object)item));
			}
		}

		public int pathCount {
			get {
				if (null == listPath)
					listPath = new List<int> ();
				return listPath.Count;
			}
		}

		public int localPathCount {
			get {
				return Mathf.Max (0, pathCount - 1);
			}
		}


		public string localPath {
			get {
				int pathCount = this.pathCount;
				if (pathCount <= 0)
					return "";
				List<int> localPath = new List<int> ();
				for (int i = 1; i < pathCount; i++)
					localPath.Add (this.listPath [i]);
				return Utils.ArrayToString (separator.ToString (), "", "", "", "", System.Array.ConvertAll (localPath.ToArray (), item => (object)item));
			}
		}

		[SerializeField]
		[DisabledFieldAttribute]
		private List <int> listPath;

		public TransformIndexPath (GameObject go, char sep = ':', Transform root = null) : this (go.transform, sep, root)
		{
			
		}

		public TransformIndexPath (Transform transform, char sep = ':', Transform root = null) : this ()
		{
			listPath = new List<int>();
			separator = sep;
			GetPath (transform,root);
		}

		public Object GetObject (Transform root)
		{
			if (!root)
				return null;

			int count = pathCount;

			Transform currentTransform = root;
			for (int i = 0; i < count; i++)
			{
				if (listPath [i] >= currentTransform.childCount)
					return null;
				currentTransform = currentTransform.GetChild (listPath [i]);

			}
			return currentTransform;
		}

		public void GetPath (Object transform, Transform limitRoot = null)
		{
			listPath.Clear ();
			if (!transform)
				return;

			Transform t = transform as Transform;
			if (!t) {
				return;
			}

			Transform currentTransform = t;
			Transform root = limitRoot;
			if (root == transform) {
				return;
			}

			listPath.Add (currentTransform.GetSiblingIndex ());

			while (currentTransform.parent != null)
			{
				currentTransform = currentTransform.parent;
				if (currentTransform == root)
					break;
//				index = Utils.StringConCat (currentTransform.GetSiblingIndex (), separator, index);
				listPath.Insert (0, currentTransform.GetSiblingIndex ());

			}
		}

		public void FromString (string path, char separator = ':')
		{
			throw new System.NotImplementedException ();
		}

		public int[] ToArray ()
		{
			return listPath.ToArray ();
		}

		public override string ToString ()
		{
			return path;
		}

	}

	[System.Serializable]
	public struct ComponentIndexPath : IStringPath
	{
		public static implicit operator string (ComponentIndexPath tiPath)
		{
			return tiPath.path;
		}

		public string localPath {
			get {
				int pathCount = this.pathCount;
				if (pathCount <= 0)
					return "";
				List<string> paths = new List<string> ();
				for (int i = 1; i < pathCount; i++)
					paths.Add (this.listPath [i].ToString ());
				paths.Add (comIndex.ToString ());
				paths.Add (comType);
				return Utils.ArrayToString (separator.ToString (), "", "", "", "", System.Array.ConvertAll (paths.ToArray (), item => (object)item));
			}
		}

		public char separator {
			get
			{
				return _sep;
			}
			set
			{
				_sep = value;
			}
		}

		[SerializeField]
		[HideInInspector]
		private char _sep;

		public string path {
			get {
				List<string> paths = new List<string> ();
				for (int i = 0; i < listPath.Count; i++)
				{
					paths.Add (listPath [i].ToString ());
				}
				paths.Add (comIndex.ToString ());
				paths.Add (comType);
				return Utils.ArrayToString (separator.ToString (), "", "", "", "", System.Array.ConvertAll (paths.ToArray (), item => (object)item));
			}
		}

		public int pathCount {
			get {
				if (null == listPath)
					listPath = new List<int> ();
				return listPath.Count;
			}
		}

		public int localPathCount {
			get {
				return Mathf.Max (0, pathCount - 1);
			}
		}

		[SerializeField]
		[DisabledFieldAttribute]
		private List <int> listPath;

		[SerializeField]
		[HideInInspector]
		private string comType;

		[SerializeField]
		[HideInInspector]
		private int comIndex;

		public ComponentIndexPath (Component com, char sep = ':', Transform root = null) : this ()
		{
			listPath = new List<int>();
			separator = sep;
			comType = "";
			List <Component> listCom = new List<Component>(com.gameObject.GetComponents<Component>());
			comIndex = -1;
			GetPath (com,root);
		}

		public Object GetObject (Transform root)
		{

			if (comIndex < 0)
				return null;

			if (!root)
				return null;

			int count = pathCount;

			Transform currentTransform = root;
			for (int i = 0; i < count; i++)
			{
				if (listPath [i] >= currentTransform.childCount)
					return null;
				currentTransform = currentTransform.GetChild (listPath [i]);

			}

			List <Component> allComponents = new List<Component> (currentTransform.GetComponents<Component> ());
			string cType = comType;

			if (comIndex < allComponents.Count)
			{
				if (allComponents [comIndex].GetType ().ToString ().Equals (cType))
					return allComponents [comIndex];
			}

			return allComponents.Find (item => item.GetType ().ToString ().Equals (cType));
		}

		public void GetPath (Object component, Transform limitRoot = null)
		{
			listPath.Clear ();
			if (!component)
				return;
			Component com = component as Component;
			if (!com)
				return;

			Transform currentTransform = com.transform;
			Transform root = limitRoot;
			if (root != com.transform) {
//				string index = currentTransform.GetSiblingIndex ().ToString ();
				listPath.Add (currentTransform.GetSiblingIndex ());
				while (currentTransform.parent != null) {
					currentTransform = currentTransform.parent;
					if (currentTransform == root)
						break;
					//				index = Utils.StringConCat (currentTransform.GetSiblingIndex (), separator, index);
					listPath.Insert (0, currentTransform.GetSiblingIndex ());
				}
			}

			this.comType = com.GetType ().ToString ();
			List <Component> listCom = new List<Component>(com.gameObject.GetComponents<Component>());
			this.comIndex = listCom.IndexOf (com);
		}

		public void FromString (string path, char separator = ':')
		{
			throw new System.NotImplementedException ();
		}

		public int[] ToArray ()
		{
			List<int> paths = new List<int> (listPath);
			paths.Add (comIndex);
			return paths.ToArray ();
		}
	}

	//========================================

	#endregion

	#region Windows API

	[System.Runtime.InteropServices.DllImport("user32.dll")]
	private static extern System.IntPtr GetActiveWindow();

	public static System.IntPtr GetWindowHandle() {
		return GetActiveWindow();
	}
	#endregion

	#region Math....
	//Math....=========================

	/// <summary>
	/// float NaN and infinity check.
	/// </summary>
	/// <returns>Return 0 if the input value is NaN or infinity, otherwise return the input.</returns>
	/// <param name="f">F.</param>
	public static float NaNAndInfinityCheck(this float f)
	{
		if (float.IsNaN (f) || float.IsInfinity (f))
			return 0f;
		return f;
	}

	/// <summary>
	/// float NaN and infinity check.
	/// </summary>
	/// <returns>Return 0 if the input value is NaN or infinity, otherwise return the input.</returns>
	/// <param name="f">F.</param>
	public static double NaNAndInfinityCheck(this double d)
	{
		if (double.IsNaN (d) || double.IsInfinity (d))
			return 0d;
		return d;
	}

	public static float GetFractor(float inputFloat)
	{
		int integerValue = Mathf.FloorToInt(inputFloat);
		return inputFloat - integerValue;
	}

	public static float SmootherStep (float t)
	{
		return t * t * t * (t * (6f * t - 15f) + 10f);
	}

	public static float SmoothStep (float t)
	{
		return t * t * (3f - 2f * t);
	}

	public static double MoveToward (double current, double dest, double step)
	{
		double output = current;
		if (current < dest)
		{
			output = current + step;
			if (output > dest)
				output = dest;
		}
		else if (current > dest)
		{
			output = current - step;
			if (output < dest)
				output = dest;
		}

		return output;
	}

	public static float SineWave (float amplitude, float frequency)
	{
		return amplitude * Mathf.Sin (2f * Mathf.PI * frequency * Time.timeSinceLevelLoad);
	}

	public static bool IsValid (this Vector3 vector)
	{
		return (!float.IsNaN (vector.x) && !float.IsInfinity (vector.x) &&
			!float.IsNaN (vector.y) && !float.IsInfinity (vector.y) &&
			!float.IsNaN (vector.z) && !float.IsInfinity (vector.z));
	}


	public static Vector3 RotatePointAroundPivot(this Vector3 point, Vector3 pivot, Vector3 angles) {
		return Quaternion.Euler(angles) * (point - pivot) + pivot;
	}

	public static Vector3 RotatePointAroundPivot(this Vector3 point, Vector3 pivot, Quaternion angles) {
		return angles * (point - pivot) + pivot;
	}
	/// <summary>
	/// Like Vector3.ProjectOnPlane but takes another point that the plane passes through (contains), like some sort of
	/// a realistic 3D plane that has position and normal.
	/// </summary>
	/// <returns>The on plane contain pivot.</returns>
	/// <param name="point">Point.</param>
	/// <param name="planePivot">Plane pivot.</param>
	/// <param name="onNormal">On normal.</param>
	public static Vector3 ProjectOn3DPlane (this Vector3 point, Vector3 planePivot, Vector3 onNormal)
	{
		return Vector3.ProjectOnPlane (point - planePivot, onNormal) + planePivot;
	}

	/// <summary>
	/// Returns the linear alignment ratio between two vector. A value of 1 means 2 vectors are pefectly aligned, 
	/// a value of -1 means 2 vectors are perfectly opposited each other, 
	/// a value of 0 means 2 vectors are pefectly perpendicular to each other.
	/// </summary>
	/// <returns>The alignment ratio.</returns>
	/// <param name="vec1">Vec1.</param>
	/// <param name="vec2">Vec2.</param>
	public static float VectorLinearAlignmentRatio (Vector3 vec1, Vector3 vec2)
	{
		if (vec1.sqrMagnitude != 1f) {
			vec1 = vec1.normalized;
		}
		if (vec2.sqrMagnitude != 1f)
		{
			vec2 = vec2.normalized;
		}
		float cos = Vector3.Dot (vec1, vec2);
		bool inverse = false;
		if (cos < 0f)
		{
			cos = Vector3.Dot (vec1, -vec2);
			inverse = true;
		}
		float sin = Mathf.Sqrt (1f - cos * cos);
		float sin45 = 0.70710678118f;
		float sqrt2 = 1.41421356237f;
		float c = 1f;
		float d = 0f;
		if (cos > sin45)
		{
			c = sin45 * (cos - sin) / (cos + sin);
			d = (sqrt2 / 2f - c) / sqrt2;
		}
		else
		{
			c = -sin45 * (cos - sin) / (cos + sin);
			d = (sqrt2 / 2f + c) / sqrt2;
		}
		if (!inverse)
			return Mathf.Clamp (d, -1f, 1f);
		else
			return Mathf.Clamp (-(1f - d), -1f, 1f);
	}


	public static double FastAcos (double x)
	{
		return (-0.69813170079773212 * x * x - 0.87266462599716477) * x + 1.5707963267948966;
	}

	public static float FastAcos (float x)
	{
		return (-0.698131700f * x * x - 0.872664625f) * x + 1.570796326f;
	}

	public static float FastAcosDeg (float x)
	{
		return FastAcos (x) * Mathf.Rad2Deg;
	}

	#endregion

	//Arrays==========================


	#region Audio

	//Audio===========================
	public static AudioClip GetRandomClip (params AudioClip[] clips)
	{
		if (clips != null)
		{
			if (clips.Length == 0)
				return null;
			return clips [Random.Range (0, clips.Length)];
		}
		return null;
	}

	public static AudioClip GetMp3Clip (string fileName)
	{
//		Mp3Sharp.Mp3Stream stream = new Mp3Sharp.Mp3Stream (fileName);

		return null;
	}

	#endregion

	#region Animation

	public static RuntimeAnimatorController GetRootController (this RuntimeAnimatorController ctrller)
	{
		RuntimeAnimatorController root = null;
		if (ctrller)
		{
			root = ctrller;
			AnimatorOverrideController ovr = root as AnimatorOverrideController;
			if (ovr)
				root = ovr.runtimeAnimatorController;
		}
		return root;
	}

	#endregion

	#region Physics
	//Raycasting======================

	public static bool CanSee(Vector3 fromPostion, Vector3 targetPostion,params GameObject[] exclude)
	{
//		List<GameObject> listExclude=new List<GameObject>(exclude);
//		Ray rayCast = new Ray(fromPostion,targetPostion-fromPostion);
//		float rayDistance = Vector3.Distance(fromPostion,targetPostion);
//		if (rayDistance == 0)
//			return true;
//		List<RaycastHit> obstacles = new List<RaycastHit>(Physics.RaycastAll(rayCast,rayDistance));
//		obstacles.RemoveAll(item => listExclude.Contains(item.collider.gameObject));
		return CanSee(fromPostion,targetPostion,-1,exclude);
	}

	public static bool CanSee(Vector3 fromPostion, Vector3 targetPostion,LayerMask scannableLayer,params GameObject[] exclude)
	{
		return CanSee(fromPostion,targetPostion,scannableLayer,true,false,exclude);
	}

	public static bool CanSee(Vector3 fromPostion, Vector3 targetPostion,LayerMask scannableLayer,bool ignoreChildren,bool debugMode,params GameObject[] exclude)
	{
		
		Ray rayCast = new Ray(fromPostion,targetPostion-fromPostion);
		float rayDistance = Vector3.Distance(fromPostion,targetPostion);
		if (rayDistance == 0)
			return true;
		if (exclude.Length>0)
		{
			if (debugMode)
				Debug.Log("Using exclude method");
//			List<RaycastHit> obstacles = new List<RaycastHit>(Physics.RaycastAll(rayCast,rayDistance,scannableLayer));
//			obstacles.RemoveAll(item => listExclude.Contains(item.collider.gameObject) || (listExclude.Contains(item.transform.root.gameObject) && ignoreChildren));
//			return obstacles.Count<=0;
			RaycastHit[] obstacles = (Physics.RaycastAll(rayCast,rayDistance,scannableLayer));

			//Search through all hit
			for (int i = 0; i < obstacles.Length; i++)
			{
				RaycastHit hit = obstacles [i];
				bool isExcluded = false;
				//Check the exclude list
				for (int j = 0; j < exclude.Length; j++)
				{
					GameObject excludedGO = exclude [j];
					if (ignoreChildren) {
						//Is an exclusion of children gameobject
						isExcluded = hit.collider.transform.IsChildOf (excludedGO.transform);
					}
					else{
						isExcluded = hit.collider.gameObject == excludedGO;
					}
//					if (isExcluded)
//						break;
				}
				if (!isExcluded)
					return false;
			}
			return true;
		}
		else
		{
			if (debugMode)
				Debug.Log("Using traditional method");
			return !Physics.Raycast (rayCast, rayDistance, scannableLayer);
		}
	}

	public static bool CanSeeRaycast (Vector3 fromPostion, Vector3 targetPostion, params GameObject[] exclude)
	{
		return CanSeeRaycast (fromPostion, targetPostion, -1, exclude);
	}

	public static bool CanSeeRaycast(Vector3 fromPostion, Vector3 targetPostion,LayerMask scannableLayer,params GameObject[] exclude)
	{
		HashSet<GameObject> listExclude = new HashSet<GameObject> ();
		if (exclude != null) {
			for (int i = 0; i < exclude.Length; i++)
				listExclude.Add (exclude [i]);
		}
		RaycastHit hit;
		Ray rayCast = new Ray(fromPostion,targetPostion-fromPostion);
		float rayDistance = Vector3.Distance(fromPostion,targetPostion);
		if (rayDistance == 0)
			return true;
		if (!Physics.Raycast(rayCast, out hit,rayDistance, scannableLayer))
		{
			return true;
		}
			
		return listExclude.Contains (hit.collider.gameObject);
	}

	#endregion

	#region Transform

	//Transform Extension=======================

	public static void DeleteAllChild(GameObject input)
	{
		if (input)
			DeleteAllChild (input.transform);
	}

	public static void DeleteAllChild(Transform input)
	{
		if (!input)
			return;
		//Delete all observe button
//		Transform[] trans = input.GetAllChildTransform();
//		for (int i=0;i<trans.Length;i++)
//		{
//			if (trans[i]!=input && trans[i])
//			{
////				BSObjectBehaviour.ReleaseCache (trans [i].gameObject);
//				if (Application.isPlaying)
//					GameObject.Destroy(trans[i].gameObject);
//				else
//					GameObject.DestroyImmediate(trans[i].gameObject);
//			}
//		}
		while (input.childCount > 0)
		{
			Transform t = input.GetChild (0);
			t.SetParent (null);
			if (Application.isPlaying)
				GameObject.Destroy (t.gameObject);
			else
				GameObject.DestroyImmediate (t.gameObject);
		}
	}

	public static bool IsChildOf(this Transform childTransform, Transform parentTransform)
	{
		if (childTransform && parentTransform)
		{
			if (childTransform.parent == parentTransform || childTransform.root == parentTransform) {
				return true;
			} else {
				Transform childsParent = childTransform.parent;
				while (childsParent)
				{
					if (childsParent == parentTransform)
						return true;
					childsParent = childsParent.parent;
				}
			}
		}
		return false;
	}

	public static Transform[] GetAllChildTransform (this Transform input)
	{
		if (input)
		{
			List<Transform> allTransform = new List<Transform>(GetAllComponentsInChildren<Transform> (input));
			allTransform.RemoveAll (item => item == input);
			return allTransform.ToArray ();
		}
		return new Transform[0];
	}

	public static bool IsSameRoot (Transform t1, Transform t2)
	{
		if (t1 == null || t2 == null)
			return false;
		return t1.root == t2.root && t1.root;
	}


	/// <summary>
	/// Like LookAt() but use different axis as LookAt axis instead of transform.forward
	/// </summary>
	/// <returns>The <see cref="UnityEngine.Quaternion"/>.</returns>
	/// <param name="transform">Transform.</param>
	/// <param name="lookAtPosition">Look at position.</param>
	/// <param name="forward">Forward.</param>
	/// <param name="upward">Upward.</param>
	public static Quaternion AxisLookAt (this Transform transform, Vector3 lookAtPosition,Axis forward, Axis upward)
	{

		transform.LookAt (lookAtPosition);

		Vector3 fwd = Vector3.zero;
		Vector3 upw = Vector3.zero;
		Vector3 right = Vector3.zero;

		switch (upward) {
		case Axis.X:
			right = transform.up;
			break;
		case Axis.Y:
			upw = transform.up;
			break;
		case Axis.Z:
			fwd = transform.up;
			break;
		case Axis.NegativeX:
			right = -transform.up;
			break;
		case Axis.NegativeY:
			upw = -transform.up;
			break;
		case Axis.NegativeZ:
			fwd = -transform.up;
			break;
		}

		switch (forward) {
		case Axis.X:
			right = transform.forward;
			break;
		case Axis.Y:
			upw = transform.forward;
			break;
		case Axis.Z:
			fwd = transform.forward;
			break;
		case Axis.NegativeX:
			right = -transform.forward;
			break;
		case Axis.NegativeY:
			upw = -transform.forward;
			break;
		case Axis.NegativeZ:
			fwd = -transform.forward;
			break;
		}
		Quaternion output = Quaternion.identity;
		output = Quaternion.LookRotation (fwd, upw);
		//		transform.rotation = output;
		return output;
	}

	public static int GetChildrenCount (this Transform transform)
	{
		int childCount = transform.childCount;
		int count = childCount;
		for (int i = 0; i < childCount; i++)
		{
			count += transform.GetChild (i).GetChildrenCount ();
		}
		return count;
	}

	public static Vector3 ComputeTorque(Transform transform, Rigidbody rigidbody, Quaternion desiredRotation){
		//q will rotate from our current rotation to desired rotation
		Quaternion q = desiredRotation * Quaternion.Inverse(transform.rotation);
		//convert to angle axis representation so we can do math with angular velocity
		Vector3 x;
		float xMag;
		q.ToAngleAxis (out xMag, out x);
		x.Normalize ();
		//w is the angular velocity we need to achieve
		Vector3 w = x * xMag * Mathf.Deg2Rad / Time.fixedDeltaTime;
		w -= rigidbody.angularVelocity;
		//to multiply with inertia tensor local then rotationTensor coords
		Vector3 wl = transform.InverseTransformDirection (w);
		Vector3 Tl;
		Vector3 wll = wl;
		wll = rigidbody.inertiaTensorRotation * wll;
		wll.Scale(rigidbody.inertiaTensor);
		Tl = Quaternion.Inverse(rigidbody.inertiaTensorRotation) * wll;
		Vector3 T = transform.TransformDirection (Tl);
		return T;
	}

	#endregion


	#region String Utils
	//String Utils=====================

	public static string DeleteLine (this string input)
	{
		int index = input.LastIndexOf ("\n");
		if (index >= 0)
			return input.Substring (0, index + 1);
		else
			return "";
	}

	public static string PrintLn (this string str, string input)
	{
		str = string.Concat (str, input, "\n");
		return str;
	}

	public static string Print (this string str, string input)
	{
		str = string.Concat (str, input);
		return str;
	}

	public static string PercentString(string inputStr, float percentage)
	{
		if (inputStr==null)
			return null;
		if (inputStr.Length==0 || percentage<=0f)
			return "";
		if (percentage>=1f)
			return inputStr;
		int pos = Mathf.RoundToInt(inputStr.Length * percentage);
		pos = Mathf.Clamp(pos,0,inputStr.Length-1);
		return inputStr.Substring(0,pos);
	}

	public static string PercentStringReverse(string inputStr, float percentage)
	{
		if (inputStr==null)
			return null;
		if (inputStr.Length==0 || percentage<=0f)
			return "";
		if (percentage>=1f)
			return inputStr;
		int pos = Mathf.RoundToInt(inputStr.Length * percentage);
		pos = Mathf.Clamp(pos,0,inputStr.Length-1);
		return inputStr.Substring(inputStr.Length-pos,pos);
	}

	public static string GenerateRandomString(int length, byte startRange, byte endRange)
	{
		string output = "";
		while (length>0)
		{
			char a = (char) Random.Range(startRange,endRange);
			output+=a;
			length--;
		}
		return output;
	}

	public static string GenerateRandomString(int length)
	{
		return GenerateRandomString(length,33,255);
	}

	public static string GenerateRandomStringWordOnly(int length)
	{
		return GenerateRandomString(length,65,122);
	}

	public static string NewLine (this string baseLog, string newLogLine, int lenghThreshold)
	{
		string output = baseLog;
		if (baseLog.Length + newLogLine.Length > lenghThreshold)
		{
			output = baseLog.Substring (newLogLine.Length);
			int nextLine = output.IndexOf ("\n");
			if (nextLine >= 0)
			{
				output = output.Substring (nextLine);
			}
		}
		return output + newLogLine + "\n";
	}


	public static byte[] EncryptTextToMemory(string Data,  byte[] Key, byte[] IV)
	{
		try
		{
			// Create a MemoryStream.
			MemoryStream mStream = new MemoryStream();

			// Create a new TripleDES object.
			TripleDES tripleDESalg = TripleDES.Create();

			// Create a CryptoStream using the MemoryStream 
			// and the passed key and initialization vector (IV).
			CryptoStream cStream = new CryptoStream(mStream, 
				tripleDESalg.CreateEncryptor(Key, IV), 
				CryptoStreamMode.Write);

			// Convert the passed string to a byte array.
			byte[] toEncrypt = Encoding.UTF8.GetBytes (Data);

			// Write the byte array to the crypto stream and flush it.
			cStream.Write(toEncrypt, 0, toEncrypt.Length);
			cStream.FlushFinalBlock();

			// Get an array of bytes from the 
			// MemoryStream that holds the 
			// encrypted data.
			byte[] ret = mStream.ToArray();

			// Close the streams.
			cStream.Close();
			mStream.Close();

			// Return the encrypted buffer.
			return ret;
		}
		catch(CryptographicException e)
		{
			Debug.Log (string.Format ("A Cryptographic error occurred: {0}", e.Message));
			return null;
		}

	}

	public static string DecryptTextFromMemory(byte[] Data,  byte[] Key, byte[] IV)
	{
		try
		{
			// Create a new MemoryStream using the passed 
			// array of encrypted data.
			MemoryStream msDecrypt = new MemoryStream(Data);

			// Create a new TripleDES object.
			TripleDES tripleDESalg = TripleDES.Create();

			// Create a CryptoStream using the MemoryStream 
			// and the passed key and initialization vector (IV).
			CryptoStream csDecrypt = new CryptoStream(msDecrypt, 
				tripleDESalg.CreateDecryptor(Key, IV), 
				CryptoStreamMode.Read);

			// Create buffer to hold the decrypted data.
			byte[] fromEncrypt = new byte[Data.Length];

			// Read the decrypted data out of the crypto stream
			// and place it into the temporary buffer.
			csDecrypt.Read(fromEncrypt, 0, fromEncrypt.Length);

			//Convert the buffer into a string and return it.
			return Encoding.UTF8.GetString (fromEncrypt);
		}
		catch(CryptographicException e)
		{
			Debug.Log (string.Format ("A Cryptographic error occurred: {0}", e.Message));
			return null;
		}
	}

	public static string[] SplitAtPositions (string s, params int[] positions)
	{
		List <string> output = new List<string> ();
		int lastIndex = 0;
		for (int i = 0;i<positions.Length; i++)
		{
			output.Add (s.Substring (lastIndex, positions [i] - lastIndex + 1));
			lastIndex = positions [i];
		}
		output.Add (s.Substring (lastIndex));
		return output.ToArray ();
	}

	public static string StringConCat(params object[] values)
	{
		StringBuilder content = new StringBuilder();
		for (int i = 0; i < values.Length;i++)
		{
			if (!string.IsNullOrEmpty(values[i].ToString()))
				content.Append(values[i].ToString());
		}
		return content.ToString();
	}

	public static string RichTextColorWrapper(string textInput, Color color)
	{
		return StringConCat("<color=#",ColorToHex(color),">",textInput,"</color>");
	}


	public static string RichTextColorWrapper(string textInput, string color)
	{
		//		return "<color=" + color + ">" + textInput + "</color>";
		return StringConCat("<color=",color,">",textInput,"</color>");
	}

	#endregion

	#region Conversion

	//Conversion=========================

	private static void BitConvertToLittleEndian (byte[] value)
	{
		if (System.BitConverter.IsLittleEndian)
		{
			System.Array.Reverse (value);
		}
	}

	/// <summary>
	/// Bitsconvert Wrapper. (Big Endian)
	/// </summary>
	/// <returns>The convert to byte.</returns>
	/// <param name="value">Value.</param>
	public static byte[] BitConvertToByte (int value)
	{
		byte[] output = System.BitConverter.GetBytes (value);
		BitConvertToLittleEndian (output);
		return output;
	}

	/// <summary>
	/// Bitsconvert Wrapper. (Big Endian)
	/// </summary>
	/// <returns>The convert to byte.</returns>
	/// <param name="value">Value.</param>
	public static byte[] BitConvertToByte (long value)
	{
		byte[] output = System.BitConverter.GetBytes (value);
		BitConvertToLittleEndian (output);
		return output;
	}

	/// <summary>
	/// Bitsconvert Wrapper. (Big Endian)
	/// </summary>
	/// <returns>The convert to byte.</returns>
	/// <param name="value">Value.</param>
	public static byte[] BitConvertToByte (char value)
	{
		byte[] output = System.BitConverter.GetBytes (value);
		BitConvertToLittleEndian (output);
		return output;
	}

	/// <summary>
	/// Bitsconvert Wrapper. (Big Endian)
	/// </summary>
	/// <returns>The convert to byte.</returns>
	/// <param name="value">Value.</param>
	public static byte[] BitConvertToByte (double value)
	{
		byte[] output = System.BitConverter.GetBytes (value);
		BitConvertToLittleEndian (output);
		return output;
	}

	/// <summary>
	/// Bitsconvert Wrapper. (Big Endian)
	/// </summary>
	/// <returns>The convert from byte.</returns>
	/// <param name="value">Value.</param>
	public static int BitConvertToInt (byte[] value)
	{
		BitConvertToLittleEndian (value);
		return System.BitConverter.ToInt32 (value, 0);
	}

	/// <summary>
	/// Bitsconvert Wrapper. (Big Endian)
	/// </summary>
	/// <returns>The convert from byte.</returns>
	/// <param name="value">Value.</param>
	public static long BitConvertToLong (byte[] value)
	{
		BitConvertToLittleEndian (value);
		return System.BitConverter.ToInt64 (value, 0);
	}

	/// <summary>
	/// Bitsconvert Wrapper. (Big Endian)
	/// </summary>
	/// <returns>The convert from byte.</returns>
	/// <param name="value">Value.</param>
	public static double BitConvertToDouble (byte[] value)
	{
		BitConvertToLittleEndian (value);
		return System.BitConverter.ToDouble (value, 0);
	}

	/// <summary>
	/// Bitsconvert Wrapper. (Big Endian)
	/// </summary>
	/// <returns>The convert from byte.</returns>
	/// <param name="value">Value.</param>
	public static char BitConvertToChar (byte[] value)
	{
		BitConvertToLittleEndian (value);
		return System.BitConverter.ToChar (value, 0);
	}

	public static void DictionaryToList<T,V> (IDictionary<T,V> dictionary, IList<KeyValuePair<T,V>> entries)
	{
		entries.Clear ();
		foreach (T key in dictionary.Keys)
		{
			entries.Add(new KeyValuePair<T,V>(key, dictionary[key]));
		}
	}

	public static List<KeyValuePair<T,V>> DictionaryToList<T,V> (IDictionary<T,V> dictionary)
	{
		List<KeyValuePair<T,V>> entries = new List<KeyValuePair<T, V>>();
		foreach (T key in dictionary.Keys)
		{
			entries.Add(new KeyValuePair<T,V>(key, dictionary[key]));
		}
		return entries;
	}

	public static void ListToDictionary <T,V> (IList<KeyValuePair<T,V>> list, IDictionary<T,V> dictionary)
	{
		dictionary.Clear ();
		foreach (KeyValuePair<T,V> keyValue in list)
		{
			if (!dictionary.ContainsKey (keyValue.Key))
				dictionary.Add (keyValue.Key, keyValue.Value);
		}
	}

	public static Dictionary<T,V> ListToDictionary <T,V> (IList<KeyValuePair<T,V>> list)
	{
		Dictionary<T,V> dictionary = new Dictionary<T, V> ();
//		foreach (KeyValuePair<T,V> keyValue in list)
//		{
//			if (!dictionary.ContainsKey (keyValue.Key))
//				dictionary.Add (keyValue.Key, keyValue.Value);
//		}
		ListToDictionary <T,V> (list, dictionary);
		return dictionary;
	}

	public enum DictionaryMergeMode
	{
		AdditionOnly,
		Override,
	}

	public static Dictionary<T,V> DictionaryMerger <T,V> (this IDictionary <T,V> dict1,IDictionary <T,V> dict2, DictionaryMergeMode mergeMode = DictionaryMergeMode.AdditionOnly)
	{
		Dictionary<T,V> mergedDict = new Dictionary<T, V> ();
		T[] dict1Keys = new T[dict1.Keys.Count];
		dict1.Keys.CopyTo (dict1Keys, 0);

		foreach (T key in dict1Keys)
		{
			mergedDict.Add (key, dict1 [key]);
		}

		T[] dict2Keys = new T[dict2.Keys.Count];
		dict2.Keys.CopyTo (dict2Keys, 0);
		foreach (T key in dict2Keys)
		{
			switch (mergeMode)
			{
			case DictionaryMergeMode.AdditionOnly:
				if (!mergedDict.ContainsKey (key))
				{
					mergedDict.Add (key, dict2 [key]);
				}
				break;
			case DictionaryMergeMode.Override:
				mergedDict.Remove (key);
				mergedDict.Add (key, dict2 [key]);
				break;
			}

		}

		return mergedDict;
	}

	#endregion


	#region Networking
	//Networking======================================

	public static uint IPToInt (string ipAdd)
	{
		System.Net.IPAddress address = System.Net.IPAddress.Parse(ipAdd);
		byte[] bytes = address.GetAddressBytes();
		System.Array.Reverse(bytes); // flip big-endian(network order) to little-endian
		uint intAddress = System.BitConverter.ToUInt32(bytes, 0);
		return intAddress;
	}

	public static string IntToIP (uint ipAdd)
	{
		byte[] bytes = System.BitConverter.GetBytes(ipAdd);
		System.Array.Reverse(bytes); // flip little-endian to big-endian(network order)
		string ipAddress = new System.Net.IPAddress(bytes).ToString();
		return ipAddress;
	}

	public static IPAddress GetBroadcastAddress(this IPAddress address, IPAddress subnetMask)
	{
		byte[] ipAdressBytes = address.GetAddressBytes();
		byte[] subnetMaskBytes = subnetMask.GetAddressBytes();

		if (ipAdressBytes.Length != subnetMaskBytes.Length)
			throw new System.ArgumentException("Lengths of IP address and subnet mask do not match.");

		byte[] broadcastAddress = new byte[ipAdressBytes.Length];
		for (int i = 0; i < broadcastAddress.Length; i++)
		{
			broadcastAddress[i] = (byte)(ipAdressBytes[i] | (subnetMaskBytes[i] ^ 255));
		}
		return new IPAddress(broadcastAddress);
	}

	public static IPAddress GetNetworkAddress(this IPAddress address, IPAddress subnetMask)
	{
		byte[] ipAdressBytes = address.GetAddressBytes();
		byte[] subnetMaskBytes = subnetMask.GetAddressBytes();

		if (ipAdressBytes.Length != subnetMaskBytes.Length)
			throw new System.ArgumentException("Lengths of IP address and subnet mask do not match.");

		byte[] broadcastAddress = new byte[ipAdressBytes.Length];
		for (int i = 0; i < broadcastAddress.Length; i++)
		{
			broadcastAddress[i] = (byte)(ipAdressBytes[i] & (subnetMaskBytes[i]));
		}
		return new IPAddress(broadcastAddress);
	}

	public static bool IsInSameSubnet(this IPAddress address2, IPAddress address, IPAddress subnetMask)
	{
		IPAddress network1 = address.GetNetworkAddress(subnetMask);
		IPAddress network2 = address2.GetNetworkAddress(subnetMask);

		return network1.Equals(network2);
	}

	public static IPAddress GetSubnetMask(IPAddress address)
	{
		foreach (NetworkInterface adapter in NetworkInterface.GetAllNetworkInterfaces())
		{
			foreach (UnicastIPAddressInformation unicastIPAddressInformation in adapter.GetIPProperties().UnicastAddresses)
			{
				if (unicastIPAddressInformation.Address.AddressFamily == AddressFamily.InterNetwork)
				{
					if (address.Equals(unicastIPAddressInformation.Address))
					{
						return unicastIPAddressInformation.IPv4Mask;
					}
				}
			}
		}
		throw new System.ArgumentException(string.Format("Can't find subnetmask for IP address '{0}'", address));
	}

	public static bool IsInSameSubnet(this string address2, string address, string subnetMask)
	{
		return IsInSameSubnet (IPAddress.Parse (address2), IPAddress.Parse (address), IPAddress.Parse (subnetMask));
	}

	#endregion


	#region Serialization
	//Serialization=========================

	public static string ObjectToXMLString(object o)
	{
		MemoryStream memoryStream = new MemoryStream();
		XmlSerializer xs = new XmlSerializer (o.GetType());
		XmlTextWriter xmlTextWriter = new XmlTextWriter ( memoryStream, Encoding.UTF8 );
		xs.Serialize ( xmlTextWriter, o);
		return Encoding.UTF8.GetString(memoryStream .ToArray());
	}

	// Convert an object to a byte array
	public static byte[] ObjectToByteArray(object obj)
	{
		if(obj == null)
			return null;
		BinaryFormatter bf = new BinaryFormatter();
		MemoryStream ms = new MemoryStream();
		bf.Serialize(ms, obj);
		return ms.ToArray();
	}
	
	// Convert a byte array to an Object
	public static object ByteArrayToObject(byte[] arrBytes)
	{
		MemoryStream memStream = new MemoryStream();
		BinaryFormatter binForm = new BinaryFormatter();
		memStream.Write(arrBytes, 0, arrBytes.Length);
		memStream.Seek(0, SeekOrigin.Begin);
		object obj = binForm.Deserialize(memStream);
		return obj;
	}

	private static Stream GenerateStreamFromString(string s)
	{
		MemoryStream stream = new MemoryStream();
		StreamWriter writer = new StreamWriter(stream);
		writer.Write(s);
		writer.Flush();
		stream.Position = 0;
		return stream;
	}

	public static object XMLStringToObject(string input,System.Type type)
	{
		try
		{
			object o = null;
			XmlSerializer xs = new XmlSerializer(type);
			Stream stream = GenerateStreamFromString(input);
			o = xs.Deserialize(stream);
			return o;
		}
		catch (System.Exception ex)
		{
			Debug.LogError(ex.ToString());
		}
		return null;
	}

	public static T XMLStringToObject<T>(string input)
	{
		object o = XMLStringToObject(input,typeof(T));
		if (o != null)
			return (T) o;
		return default(T);
	}

	public static string ArrayToString (object[] arrayObject)
	{
		return ArrayToString (", ", "(", ")", arrayObject);
	}

	public static string ArrayToString(string separatorString, object[] arrayObject)
	{
		return ArrayToString (separatorString, "(", ")", arrayObject);
	}

	public static string ArrayToString(string separatorString, string openString, string closeString, object[] arrayObject)
	{
		return ArrayToString(separatorString, openString, closeString, "", "", arrayObject);
	}

	public static string ArrayToString(string separatorString, string openString, string closeString, string itemOpenCover, string itemCloseCover, object[] arrayObject)
	{
		StringBuilder output = new StringBuilder();
		if (arrayObject.Length == 0)
			return "";
		if (arrayObject.Length == 1)
		{
			return openString + itemOpenCover + arrayObject[0].ToString() + itemCloseCover + closeString;
		}
		else
		{
			int i = 0;
			output.Append(openString);
			while (i < arrayObject.Length)
			{
				if (i != 0)
				{
					output.Append(separatorString);
				}
				output.Append(itemOpenCover);
				output.Append(arrayObject[i].ToString());
				output.Append(itemCloseCover);
				i++;
			}
			output.Append(closeString);
			return output.ToString();
		}

	}

	public static string ColorToHex(Color32 color)
	{

		string hex = StringConCat(color.r.ToString("X2"), color.g.ToString("X2"), color.b.ToString("X2"));
		return hex;
	}

	public static Color HexToColor(string hex)
	{
		hex = hex.ToUpper ();
		hex = hex.Replace ("0x", "");//in case the string is formatted 0xFFFFFF
		hex = hex.Replace ("#", "");//in case the string is formatted #FFFFFF
		byte r = byte.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
		byte g = byte.Parse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
		byte b = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
		return new Color32(r, g, b, 255);
	}

	/// <summary>
	/// Convert a value to a desired type T.
	/// </summary>
	/// <typeparam name="T">Desired type</typeparam>
	/// <param name="value">Input object</param>
	/// <returns>default (T), otherwise the converted value</returns>
	public static T ConvertValue<T>(object value) 
	{
		if (value != null)
		{
			if (typeof (T).IsAssignableFrom (value.GetType()))
			{
				return (T)value;
			}
		}
		return default (T);
	}

	#endregion

	//=====================Language===========================



	#region Scene

	//Scene path manipulator========================

	public static string GetSceneShortPath(string path)
	{
		try
		{
			return Path.GetFileNameWithoutExtension(path);

		} catch (IOException ex)
		{
			Debug.LogError(ex.ToString());
			return "";
		}

	}

	public static string GetSceneCorrectPath(string path)
	{
		try
		{
			string output = path;
			string rootDirectory = path.Substring(0, path.IndexOf("/") + 1);
			rootDirectory = rootDirectory.Replace("/","");
			if (rootDirectory.ToLower ().Equals("assets"))
			{
				Regex regex = new Regex(Regex.Escape("Assets/"));
				output = regex.Replace (path,"",1);
			}
//			Debug.Log("Ext: " + Path.GetExtension(output));

			if (Path.GetExtension(output).ToLower().Contains("unity"))
				output = (Path.GetDirectoryName(output) + "/" + Path.GetFileNameWithoutExtension(output));
			return output;
//			int index = path.ToLower ().IndexOf ("assets/");
//			if (index >= 0)
//			{
//				output = path.Substring ("assets/".Length);
//			}
//
//			return Path.GetFileNameWithoutExtension (output);

		} catch (IOException ex)
		{
			Debug.LogError(ex.ToString());
			return "";
		}

	}



	public static string AddOrdinal(int num)
	{
		if( num <= 0 ) return num.ToString();
		
		switch(num % 100)
		{
		case 11:
		case 12:
		case 13:
			return num + "th";
		}
		
		switch(num % 10)
		{
		case 1:
			return num + "st";
		case 2:
			return num + "nd";
		case 3:
			return num + "rd";
		default:
			return num + "th";
		}
		
	}




	#endregion

	#region File

	public static string AppExecFolder
	{
		get
		{
			return Directory.GetParent (Application.dataPath).FullName;
		}
	}

	#endregion



	#region Bounds
	//Bounds==========================================

	public static Vector3 GetBoundPoint(this Collider collider, BoundPosition pos)
	{
		Bounds bounds = collider.bounds;
		switch (pos)
		{
		case BoundPosition.UPPER_CENTER:
			return new Vector3(bounds.center.x,bounds.max.y,bounds.center.z);
		case BoundPosition.LOWER_CENTER:
			return new Vector3(bounds.center.x,bounds.min.y,bounds.center.z);
		default:
			return bounds.center;
		}
	}

	/// <summary>
	/// Same as Collider.ClosestPointOnBounds, but return an inside point instead
	/// </summary>
	/// <returns>The point under bound.</returns>
	/// <param name="collider">Collider.</param>
	/// <param name="position">Position.</param>
	/// <param name="shrink">Offset from collider surface.</param>
	public static Vector3 ClosestPointUnderBound (this Collider collider, Vector3 position,float shrink)
	{
		Vector3 pointOnBound = collider.ClosestPointOnBounds (position);
		shrink = Mathf.Clamp (shrink, 0, Mathf.Infinity);
		return Vector3.MoveTowards(pointOnBound,collider.bounds.center,shrink);
	}

	public static Bounds GetEncapsulateBound (GameObject _prefab)
	{

		if (!_prefab)
			return new Bounds ();

		Vector3 postion = _prefab.transform.position;
		Quaternion rotation = _prefab.transform.rotation;

		GameObject go = _prefab;

		go.transform.position = Vector3.zero;
		go.transform.rotation = Quaternion.identity;

		List<Renderer> allRenderers = new List<Renderer> (Utils.GetAllComponentsInChildren<MeshRenderer> (go));

		allRenderers.AddRange (Utils.GetAllComponentsInChildren<SkinnedMeshRenderer> (go));

		Bounds prefabBound = new Bounds (go.transform.position, Vector3.zero);
		if (go.GetComponent<Renderer> ())
			prefabBound = go.GetComponent<Renderer> ().bounds;

		foreach (Renderer r in allRenderers) {
			prefabBound.Encapsulate (r.bounds);
		}
		prefabBound.center = new Vector3 ();
		//					Debug.Log("Renderer found: " + allRenderers.Count);

		go.transform.position = postion;
		go.transform.rotation = rotation;

		return prefabBound;
	}

	#endregion

	#region Components & GameObject

	//Components========================================

	private static void ProcessChildGetComponent<T>(Transform aObj, ref List<T> aList) where T : Component
	{
		T[] c = aObj.GetComponents<T>();
		if (c != null)
			aList.AddRange(c);
		foreach (Transform child in aObj)
			ProcessChildGetComponent<T> (child, ref aList);
	}

	/// <summary>
	/// Gets all the components of type T in this transform and its children.
	/// </summary>
	/// <returns>The components in this GameObject and its children.</returns>
	/// <param name="aObj">Transform to query.</param>
	/// <typeparam name="T">Component type.</typeparam>
	public static T[] GetAllComponentsInChildren<T>(this Transform aObj) where T : Component
	{
		List<T> result = new List<T>();
		ProcessChildGetComponent<T> (aObj, ref result);
		return result.ToArray();
	}

	/// <summary>
	/// Gets all the components of type T in this gameobject and its children.
	/// </summary>
	/// <returns>The components in this GameObject and its children.</returns>
	/// <param name="aObj">GameObject to query.</param>
	/// <typeparam name="T">Component type.</typeparam>
	public static T[] GetAllComponentsInChildren<T>(this GameObject aObj) where T : Component
	{
		return GetAllComponentsInChildren<T> (aObj.transform);
	}


	public static Transform LocalNamePath2Transform (Transform root, string namePath, char separator = ':')
	{
		return NamePath2Transform (root, namePath, separator, false);
	}

	public static Transform NamePath2Transform (Transform root, string namePath, char separator = ':', bool firstNameIsRoot = true)
	{
		if (!root)
			return null;
//		List<string> paths = new List<string> (namePath.Split (separator));
//		if (firstNameIsRoot)
//		{
//			if (!paths [0].Equals (root.name))
//				return null;
//			paths.RemoveAt (0);
//		}
//		int count = paths.Count;
//		Transform t = null;
//		Transform currentTransform = root;
//		for (int i = 0; i < count; i++)
//		{
//			t = currentTransform.Find (paths [i]);
//			if (!t)
//				return null;
//			currentTransform = t;
//		}
//
//		return t;
		return NamePath2Transform (root, firstNameIsRoot, namePath.Split (separator));
	}

	public static Transform NamePath2Transform (Transform root, bool firstNameIsRoot, params string[] namePath)
	{
		List<string> paths = new List<string> (namePath);
		if (firstNameIsRoot)
		{
			if (!paths [0].Equals (root.name))
				return null;
			paths.RemoveAt (0);
		}
		int count = paths.Count;
		Transform t = null;
		Transform currentTransform = root;
		for (int i = 0; i < count; i++)
		{
			t = currentTransform.Find (paths [i]);
			if (!t)
				return null;
			currentTransform = t;
		}

		return t;
	}


	public static string GetTransformLocalNamePath (Transform transform)
	{
		return GetTransformNamePath (transform, ':', false);
	}

	public static string GetTransformNamePath (Transform transform, char separator = ':', bool includeRoot = true)
	{
		if (!transform)
			return "";
		Transform currentTransform = transform;
		Transform root = transform.root;
		if (root == transform) {
			if (includeRoot)
				return currentTransform.name;
			return "";
		}
		string name = currentTransform.name;
		while (currentTransform.parent != null)
		{
			currentTransform = currentTransform.parent;
			if (currentTransform == root && !includeRoot)
				break;
			name = Utils.StringConCat (currentTransform.name, separator, name);
		}
		return name;
	}

	public static string GetLocalComponentNamePath (Component component, char pathSeparator = ':')
	{
		return GetComponentNamePath (component, pathSeparator, false);
	}

	public static string GetComponentNamePath (Component component, char pathSeparator = ':', bool includeRoot = true)
	{
		if (!component)
			return "";
		
		string name = GetTransformNamePath (component.transform, pathSeparator, includeRoot);
		if (string.IsNullOrEmpty (name))
			return "";
		int index = new List <Component> (component.GetComponents (typeof(Component))).IndexOf (component);
		return Utils.StringConCat (name, pathSeparator, index, pathSeparator, component.GetType ());
	}

	public static Component LocalNamePath2Component (Component root, string namePath, char separator = ':')
	{
		return NamePath2Component (root, namePath, separator, false);
	}

	public static Component NamePath2Component (Component root, string namePath, char separator = ':', bool firstNameIsRoot = true)
	{
		if (!root)
			return null;
		List<string> paths = new List<string> (namePath.Split (separator));
		if (paths.Count < 2 || (firstNameIsRoot && paths.Count < 3))
			return null;
		System.Type comType = System.Type.GetType (paths [paths.Count - 1]);
		int comIndex = int.Parse (paths [paths.Count - 2]);
		paths.RemoveRange (paths.Count - 2, 2);
		Transform t = NamePath2Transform (root.transform, firstNameIsRoot, paths.ToArray ());
		if (t)
		{
			Component[] coms = t.GetComponents (typeof(Component));
			if (comIndex >= 0 && comIndex <coms.Length)
			{
				Component c = coms [comIndex];
				if (c)
				{
					if (c.GetType () == comType)
						return c;
				}
			}
		}
		return null;
	}

	public static GameObject FindGameObjectByLocalIndexPath (this Transform obj, string namePath, char separator = ':', bool firstIndexIsRoot = true)
	{
		return FindGameObjectByIndexPath (obj, namePath, separator, false);
	}

	public static GameObject FindGameObjectByIndexPath (this Transform root, string namePath, char separator = ':', bool firstIndexIsRoot = true)
	{
		return FindGameObjectByIndexPath (root, firstIndexIsRoot, namePath.Split (separator));
	}

	public static GameObject FindGameObjectByIndexPath (this Transform root, bool firstIndexIsRoot, params string[] namePath)
	{
		if (!root)
			return null;
		List<string> strPath = new List<string> (namePath);
		List <int> paths = new List<int> ();

		for (int i = 0; i < strPath.Count; i++)
		{
			int index = -1;
			if (int.TryParse (strPath [i], out index))
			{
				paths.Add (index);
			}
			else
			{
				return null;
			}
		}
		if (firstIndexIsRoot)
		{
			if (!paths [0].Equals (root.GetSiblingIndex ()))
				return null;
			paths.RemoveAt (0);
		}

		Transform t = null;
		Transform currentTransform = root;
		for (int i = 0; i < paths.Count; i++)
		{
			if (paths [i] >= currentTransform.childCount)
				return null;
			currentTransform = currentTransform.GetChild (paths [i]);

		}
		return currentTransform.gameObject;
	}

	public static string GetLocalTransformIndexPath (this Transform transform, Transform limitRoot = null, char separator = ':')
	{
		return GetTransformIndexPath (transform, limitRoot, separator, false);
	}

	public static string GetTransformIndexPath (this GameObject go, Transform limitRoot = null, char separator = ':', bool includeRoot = true)
	{
		if (!go)
			return "";
		return GetTransformIndexPath (go.transform, limitRoot, separator, includeRoot);
	}

	public static string GetTransformIndexPath (this Transform transform, Transform limitRoot = null, char separator = ':', bool includeRoot = true)
	{
		if (!transform)
			return "";
		
		Transform currentTransform = transform;
		Transform root = transform.root;
		if (limitRoot)
			root = limitRoot;
		if (root == transform) {
			if (includeRoot)
				return currentTransform.GetSiblingIndex ().ToString ();
			return "";
		}
		string index = currentTransform.GetSiblingIndex ().ToString ();
		while (currentTransform.parent != null)
		{
			currentTransform = currentTransform.parent;
			if (currentTransform == root && !includeRoot)
				break;
			index = Utils.StringConCat (currentTransform.GetSiblingIndex (), separator, index);

		}
		return index;
	}

	public static Component FindComponentByLocalIndexPath (this GameObject gameObject, string indexPath, char separator = ':', bool firstIndexIsRoot = true)
	{
		return FindComponentByIndexPath (gameObject, indexPath, separator, false);
	}

	public static Component FindComponentByIndexPath (this GameObject gameObject, string indexPath, char separator = ':', bool firstIndexIsRoot = true)
	{
		if (!gameObject)
			return null;
		List<string> paths = new List<string> (indexPath.Split (separator));
		if (paths.Count < 2 || (firstIndexIsRoot && paths.Count < 3))
			return null;
		string comType =  (paths [paths.Count - 1]);
		int comIndex = int.Parse (paths [paths.Count - 2]);
		paths.RemoveRange (paths.Count - 2, 2);
		GameObject go = gameObject.transform.FindGameObjectByIndexPath (firstIndexIsRoot, paths.ToArray ());
		if (!go)
			return null;
		List<Component> listCom = new List<Component> (go.GetComponents<Component> ());
		if (comIndex >= 0 && comIndex < listCom.Count)
		{
			if (listCom [comIndex].GetType ().ToString ().Equals (comType))
				return listCom [comIndex];
		}

		return listCom.Find (item => item.GetType ().ToString ().Equals (comType));

	}

	public static string GetLocalComponentIndexPath (this Component com, Transform limitRoot = null, char separator = ':')
	{
		return GetComponentIndexPath (com, limitRoot, separator, false);
	}

	public static string GetComponentIndexPath (this Component com, Transform limitRoot = null, char separator = ':', bool includeRoot = true)
	{
		if (!com)
			return "";
		string indexPath = com.transform.GetTransformIndexPath (limitRoot, separator, includeRoot);
		List <Component> listCom = new List<Component> (com.GetComponents<Component> ());
		int index = listCom.IndexOf (com);
		if (index < 0)
			return "";
		return StringConCat (indexPath, separator, index, separator, com.GetType ());
	}

	/// <summary>
	/// Determines if layer maskB is from the specified layer maskA.
	/// </summary>
	/// <returns><c>true</c> if layer maskB is from the specified layer maskA; otherwise, <c>false</c>.</returns>
	/// <param name="maskA">Mask a.</param>
	/// <param name="maskB">Mask b.</param>
	public static bool IsLayerMask(this LayerMask maskA,int maskB)
	{
//		return maskB == (maskB | (1 << maskA));
		return (maskA.value & (1 << maskB)) > 0;
	}

	//GameObjects===============================================
	public static bool IsTrueNull (this Object obj)
	{
		return (object)obj == null;
	}

	#endregion

	#region Terrains

	//Terrains=============================

	public static Vector2 AlphaMapPosToHeightMapPos (this TerrainData terrainData, Vector2 alphaMapPos)
	{
		return new Vector2 (alphaMapPos.x / terrainData.alphamapWidth * terrainData.heightmapWidth, alphaMapPos.y / terrainData.alphamapHeight * terrainData.heightmapHeight);
	}

	public static Vector2 AlphaMapPosToHeightMapPos (this TerrainData terrainData, int x, int y)
	{
		return AlphaMapPosToHeightMapPos (terrainData, new Vector2 (x,y));
	}

	public static Vector2 AlphaMapPosToHeightMapPos (this TerrainData terrainData, float x, float y)
	{
		return AlphaMapPosToHeightMapPos (terrainData, new Vector2 (x,y));
	}

	public static Vector2 HeightMapPosToAlphaMapPos (this TerrainData terrainData, Vector2 heightMapPos)
	{
		return new Vector2 (heightMapPos.x / terrainData.heightmapWidth * terrainData.alphamapWidth, heightMapPos.y / terrainData.heightmapHeight * terrainData.alphamapHeight);
	}

	/// <summary>
	/// Convert world position to terrain position. (width, depth - or height map value, height)
	/// </summary>
	/// <returns>The position to terrain.</returns>
	/// <param name="terrain">Terrain.</param>
	/// <param name="position">Position.</param>
	public static Vector3 WorldPosToTerrainAlphaMap (this Terrain terrain, Vector3 position, bool useTerrainSurface = false)
	{
		Vector3 pos = Vector3.zero;

		if (terrain)
		{
			TerrainData terrainData = terrain.terrainData;
			if (terrainData)
			{
				Vector3 relativePos = position - terrain.transform.position;
				pos.x = relativePos.x / terrainData.size.x * terrainData.alphamapWidth;
				pos.z = relativePos.z / terrainData.size.z * terrainData.alphamapHeight;
				Vector3 heightMapPos = Vector3.zero;
				heightMapPos.x = relativePos.x / terrainData.size.x * (terrainData.heightmapWidth);
				heightMapPos.z = relativePos.z / terrainData.size.z * (terrainData.heightmapHeight);
				if (!useTerrainSurface) {
					pos.y = relativePos.y - terrainData.GetHeight ((int)heightMapPos.x, (int)heightMapPos.z);
				}
				else
				{
					pos.y = terrainData.GetHeight ((int)heightMapPos.x, (int)heightMapPos.z);
				}
			}
		}
		return pos;
	}


	public static Vector3 TerrainPosToWorldHeightMap (this Terrain terrain, int posX, int posY)
	{
		Vector3 pos = new Vector3 ();

		if (terrain)
		{
			TerrainData terrainData = terrain.terrainData;
			if (terrainData)
			{
				pos.x = posX / terrainData.heightmapWidth * terrainData.size.x;
				pos.z = posY / terrainData.heightmapHeight * terrainData.size.z;
				pos.y = terrainData.GetHeight (posX, posY);
			}
		}

		return pos + terrain.transform.position;
	}

	public static Vector2 WorldScaleToTerrain(this Terrain terrain, float xValue, float yValue)
	{
		Vector2 unit = new Vector2();

		if (terrain)
		{
			TerrainData terrainData = terrain.terrainData;
			if (terrainData)
			{
				unit.x = xValue/terrainData.size.x * terrainData.alphamapWidth;
				unit.y = xValue/terrainData.size.z * terrainData.alphamapHeight;
			}
		}
		return unit;
	}
		
	public static void DrawOnTerrain(this Terrain terrain, Vector3 worldPosition, int splatMapIndex , Vector2 worldSizeXY)
	{
		if (terrain)
		{
			TerrainData terrainData = terrain.terrainData;
			if (terrainData)
			{
				if (splatMapIndex < terrainData.alphamapLayers)
				{
					Vector2 terrainPos = terrain.WorldPosToTerrainAlphaMap(worldPosition);
					Vector2 terrainSize = terrain.WorldScaleToTerrain(worldSizeXY.x,worldSizeXY.y);
					int width = Mathf.Max((int) terrainSize.x,1);
					int height = Mathf.Max((int) terrainSize.y,1);
					float[,,] splatData = terrainData.GetAlphamaps((int) terrainPos.x, (int) terrainPos.y, width, height);
					for (int i = 0;i<terrainData.alphamapLayers;i++)
					{
						for (int x = 0;x<width;x++)
						{
							for (int y = 0;y<height;y++)
							{
								if (i == splatMapIndex)
									splatData[x,y,i] = 1;
								else
									splatData[x,y,i] = 0;
							}
						}
					}
					terrainData.SetAlphamaps((int) terrainPos.x, (int) terrainPos.y,splatData);
				}
			}
		}
	}

	/// <summary>
	/// Gets the highest splash map (alpha map) of a specific point on the terrain. It will check for highest value of the splash prototype and return the prototype index
	/// </summary>
	/// <returns>The highest splash map.</returns>
	/// <param name="terrainData">Terrain data.</param>
	/// <param name="positionWH">Position. (width, height)</param>
	/// <param name="height">Height Position.</param>
	/// <param name="heightThreshold">Height threshold.</param>
	public static int GetHighestSplashMap (this TerrainData terrainData, Vector2 positionWH, float height = 0, float heightThreshold = -1)
	{
		return GetHighestSplashMap (terrainData, (int)positionWH.x, (int)positionWH.y, height, heightThreshold);
	}

	/// <summary>
	/// Gets the highest splash map (alpha map) of a specific point on the terrain. It will check for highest value of the splash prototype and return the prototype index
	/// </summary>
	/// <returns>The highest splash map.</returns>
	/// <param name="terrainData">Terrain data.</param>
	/// <param name="position">Position. (width, depth, height) </param>
	/// <param name="height">Height Position.</param>
	/// <param name="heightThreshold">Height threshold.</param>
	public static int GetHighestSplashMap (this TerrainData terrainData, Vector3 positionWDH, float heightThreshold = -1)
	{
		return GetHighestSplashMap (terrainData, (int)positionWDH.x, (int)positionWDH.z, positionWDH.y, heightThreshold);
	}

	/// <summary>
	/// Gets the highest splash map (alpha map) of a specific point on the terrain. It will check for highest value of the splash prototype and return the prototype index
	/// </summary>
	/// <returns>The highest splash map index. -1 if none</returns>
	/// <param name="terrainData">Terrain data.</param>
	/// <param name="x">The x coordinate.</param>
	/// <param name="y">The y coordinate.</param>
	/// <param name="height">Height Position.</param>
	/// <param name="heightThreshold">Height threshold. If the height map value at designated position is higher than this heightThreshold, -1 will be returned.</param>
	public static int GetHighestSplashMap (this TerrainData terrainData, int x, int y, float height = 0, float heightThreshold = -1)
	{
		if (terrainData == null)
			return -1;

		if (terrainData.alphamapLayers <= 0)
			return -1;

		int posX = Mathf.Clamp (x, 0, terrainData.alphamapWidth - 1);
		int posY = Mathf.Clamp (y, 0, terrainData.alphamapHeight - 1);

		if (posX != x || posY != y)
			return -1;

		float[,,] alphaMap = terrainData.GetAlphamaps (posX, posY, 1,1);

		float alphaMapHeightValue = -1;
		Vector2 heightMapPos = terrainData.AlphaMapPosToHeightMapPos (posX, posY);
		float heightValue = terrainData.GetHeight ((int)heightMapPos.x, (int)heightMapPos.y);
		int splashMapIndex = -1;
		bool canCheckSplatMap = height - heightValue <= heightThreshold || height <= 0 || heightThreshold < 0;
		if (canCheckSplatMap) {
			for (int i = 0; i < terrainData.alphamapLayers; i++) {
				if (alphaMap [0, 0, i] > alphaMapHeightValue) {
					alphaMapHeightValue = alphaMap [0, 0, i];
					splashMapIndex = i;
				}
			}
		}
//		Debug.Log (height + " - " + heightValue + " = " + (height - heightValue) + "/" + heightThreshold + " bool " + canCheckSplatMap + " value " + splashMapIndex);
		return splashMapIndex;
	}

	#endregion

	#region Reflections

	//Reflection=====================================

	public static object CopyFields (object source, object dest)
	{
//		if (source == null)
//		{
//			throw new System.NullReferenceException ("Source object is null!");
//		}
//		if (dest == null)
//		{
//			throw new System.NullReferenceException ("Destination object is null!");
//		}
//		if (!dest.GetType ().IsAssignableFrom(source.GetType ()))
//		{
//			throw new System.ArgumentException ("Incompatible type between " + source.ToString () + "(" + source.GetType().ToString() + ") and " + dest.ToString() + "(" + dest.GetType().ToString() + ")" );
//		}
//
//		List<FieldInfo> sourceFields = new List<FieldInfo>(source.GetType ().GetFields ());
//		List<FieldInfo> destFields = new List<FieldInfo>(dest.GetType ().GetFields ());
//		foreach (FieldInfo sf in sourceFields)
//		{
//			FieldInfo df = destFields.Find (item => item.Name.Equals (sf.Name));
//			if (df != null)
//			{
//				df.SetValue (dest, sf.GetValue (source));
//			}
//		}
		return CopyFields (ref source, ref dest);
	}

	public static object CopyFields (ref object source, ref object dest)
	{
		if (source == null)
		{
			throw new System.NullReferenceException ("Source object is null!");
		}
		if (dest == null)
		{
			throw new System.NullReferenceException ("Destination object is null!");
		}
		if (!dest.GetType ().IsAssignableFrom(source.GetType ()))
		{
			throw new System.ArgumentException ("Incompatible type between " + source.ToString () + "(" + source.GetType().ToString() + ") and " + dest.ToString() + "(" + dest.GetType().ToString() + ")" );
		}

		List<FieldInfo> sourceFields = new List<FieldInfo>(source.GetType ().GetFields (BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public));
		List<FieldInfo> destFields = new List<FieldInfo>(dest.GetType ().GetFields (BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public));
		foreach (FieldInfo sf in sourceFields)
		{
//			Debug.Log ("Serializing " + sf.Name);
			FieldInfo df = destFields.Find (item => item.Name.Equals (sf.Name));
			if (df != null)
			{
				df.SetValue (dest, sf.GetValue (source));
//				Debug.Log ("Copied " + df.Name);
			}
		}

		return dest;
	}

	public static object CopyFieldsNoError (object source, object dest)
	{
		return CopyFieldsNoError (ref source, ref dest, false);
	}

	public static object CopyFieldsNoError (object source, object dest, bool dumpError)
	{
		return CopyFieldsNoError (ref source, ref dest, dumpError);
	}

	public static object CopyFieldsNoError (ref object source, ref object dest)
	{
		return CopyFieldsNoError (ref source, ref dest, false);
	}

	public static object CopyFieldsNoError (ref object source, ref object dest, bool dumpError)
	{
		try
		{
			return CopyFields (ref source, ref dest);
		}
		catch (System.Exception ex)
		{
			if (dumpError)
				Debug.LogError (ex.ToString ());
		}
		return dest;
	}

	//static class Nameof<T>
	//{
	//	public static string Property<TProp> (Expression<System.Func<T, TProp>> expression)
	//	{
	//		var body = expression.Body as MemberExpression;
	//		if (body == null)
	//			throw new System.ArgumentException ("'expression' should be a member expression");
	//		return body.Member.Name;
	//	}
	//}

	/// <summary>
	/// Gets the copy of component and adds it to the current gameobject.
	/// </summary>
	/// <returns>The copy of.</returns>
	/// <param name="comp">Comp.</param>
	/// <param name="other">Other.</param>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	public static T GetCopyOf<T>(this Component comp, T other) where T : Component
	{
		System.Type type = comp.GetType();
		if (!typeof(T).IsAssignableFrom(type)) 
			return null; // type mis-match
//		BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Default | BindingFlags.DeclaredOnly;
		PropertyInfo[] pinfos = type.GetProperties();
		foreach (var pinfo in pinfos) {
			if (pinfo.CanWrite) {
				try {
					pinfo.SetValue(comp, pinfo.GetValue(other, null), null);
				}
				catch { } // In case of NotImplementedException being thrown. For some reason specifying that exception didn't seem to catch it, so I didn't catch anything specific.
			}
		}
		FieldInfo[] finfos = type.GetFields();
		foreach (var finfo in finfos) {
			finfo.SetValue(comp, finfo.GetValue(other));
		}
		return comp as T;
	}

	public static T AddComponent<T>(this GameObject go, T toAdd) where T : Component
	{
		return go.AddComponent<T>().GetCopyOf(toAdd) as T;
	}

	#endregion

	#region Camera
	//Camera====================================================
	public static T[] GetBehaviourFromAllCamera<T> ()
	{
		System.Collections.Generic.List<T> list = new System.Collections.Generic.List<T> ();
		Camera[] cameras = Camera.allCameras;
		for (int i = 0; i < cameras.Length; i++)
		{
			list.AddRange (cameras [i].GetComponents<T> ());
		}
		return list.ToArray ();
	}
		
	public static bool IsBoundVisibleOnCamera (Bounds b, int objLayer = ~0, Camera cam = null)
	{
		Plane[] camPlanes = new Plane[6];
		bool queryAllCam = cam == null;
		if (queryAllCam)
		{
			Camera[] allCamera = Camera.allCameras;
			for (int i = 0; i < allCamera.Length; i++)
			{
				if (IsLayerMask (allCamera [i].cullingMask, objLayer) || objLayer < 0) {
					camPlanes = GeometryUtility.CalculateFrustumPlanes (allCamera [i]);
					if (GeometryUtility.TestPlanesAABB (camPlanes, b))
						return true;
				}
			}
		}
		else {
			if (IsLayerMask (cam.cullingMask, objLayer) || objLayer < 0) {
				camPlanes = GeometryUtility.CalculateFrustumPlanes (cam);
				return GeometryUtility.TestPlanesAABB (camPlanes, b);
			}
		}
		return false;

	}

	public static bool IsBoundVisibleOnCamera (Vector3 position, Vector3 size, int objLayer = ~0, Camera cam = null)
	{
		return IsBoundVisibleOnCamera (new Bounds (position, size), objLayer, cam);
	}

	#endregion

	#region UI helpers

	//GUI helpers===============================================

	public static Vector2 GetSizeRectTransform(RectTransform transform)
	{
		Vector3[] corner = new Vector3[4];
		transform.GetWorldCorners(corner);
		return new Vector2(Vector3.Distance(corner[0],corner[3]),Vector3.Distance(corner[0],corner[1]));
	}

	public static void AddEventTriggerListener (this EventTrigger trigger, EventTriggerType eventType, System.Action<BaseEventData> callback)
	{
		EventTrigger.Entry entry = new EventTrigger.Entry();
		entry.eventID = eventType;
		entry.callback = new EventTrigger.TriggerEvent();
		entry.callback.AddListener(new UnityEngine.Events.UnityAction<BaseEventData>(callback));
		trigger.triggers.Add(entry);
	}

	public static Rect GetDrawLabelRect(Camera cam, Vector3 labelWorldPosition, int width, int height)
	{
		Vector3 lblPos = cam.WorldToScreenPoint(labelWorldPosition);
		Rect labelRect = new Rect();
		labelRect.size = new Vector2 (width,height);
		labelRect.position = new Vector2 (Mathf.Clamp (lblPos.x - labelRect.width * 0.5f, 0f, cam.pixelWidth - labelRect.width),
			Mathf.Clamp (cam.pixelHeight - lblPos.y - labelRect.height * 0.5f, 0f, cam.pixelHeight - labelRect.height));

		if (Vector3.Angle(cam.transform.forward,labelWorldPosition - cam.transform.position) > cam.fieldOfView)
		{
			//			if (Vector3.Angle(transform.right,labelWorldPosition - transform.position)>Vector3.Angle(transform.up,labelWorldPosition - transform.position))
			//			{
			if (Vector3.Angle (cam.transform.right, labelWorldPosition - cam.transform.position) < 90f)
				labelRect.x = cam.pixelWidth - labelRect.width;
			else
				labelRect.x = 0;
			//			}
			//			else
			//			{
			//				if (Vector3.Angle(transform.up,labelWorldPosition - transform.position)>90f)
			//					labelRect.y = ObservingCamera.pixelHeight-labelRect.height;
			//				else
			//					labelRect.y = 0;
			//			}
		}
		return labelRect;
	}
	/// <summary>
	/// Scale a rectangle using its center as pivot.
	/// </summary>
	/// <returns>Scaled rect.</returns>
	/// <param name="rect">Rect.</param>
	/// <param name="newWidth">New width.</param>
	/// <param name="newHeight">New height.</param>
	public static Rect CenterRectScale (Rect rect, int newWidth, int newHeight)
	{
		CenterRectScaleNonAlloc (ref rect, newWidth, newHeight);
		return rect;
//		newWidth = Mathf.Abs (newWidth);
//		newHeight = Mathf.Abs (newHeight);
//
//		Vector2 center = rect.center;
//
//		rect.size = new Vector2 (newWidth, newHeight);
//		rect.center = center;
//		return rect;
	}

	/// <summary>
	/// Scale a rectangle using its center as pivot.
	/// </summary>
	/// <returns>Scaled rect.</returns>
	/// <param name="rect">Rect.</param>
	/// <param name="newWidth">New width.</param>
	/// <param name="newHeight">New height.</param>
	public static void CenterRectScaleNonAlloc (ref Rect rect, int newWidth, int newHeight)
	{
		newWidth = Mathf.Abs (newWidth);
		newHeight = Mathf.Abs (newHeight);

		Vector2 center = rect.center;

		rect.size = new Vector2 (newWidth, newHeight);
		rect.center = center;
	}

	/// <summary>
	/// Scale a rectangle using its center as pivot.
	/// </summary>
	/// <param name="rect">Rect.</param>
	/// <param name="size">Size.</param>
	public static void CenterRectScaleNonAlloc (ref Rect rect, Vector2 size)
	{
		CenterRectScaleNonAlloc (ref rect, (int)size.x, (int)size.y);
	}

	/// <summary>
	/// Scale a rectangle using its center as pivot.
	/// </summary>
	/// <returns>Scaled rect.</returns>
	/// <param name="rect">Rect.</param>
	/// <param name="newWidth">New width.</param>
	/// <param name="newHeight">New height.</param>
	public static Rect CenterRectScale (Rect rect, Vector2 size)
	{
		return CenterRectScale (rect, (int)size.x, (int)size.y);

		//		return new Rect (center.x - newWidth * 0.5f, center.y - newHeight * 0.5f, newWidth, newHeight);
	}

	public static Rect DrawLabelInfo(Camera cam, Vector3 labelWorldPosition,GUIContent content, int width, int height)
	{
		return DrawLabelInfo (cam, labelWorldPosition, content, GUI.skin.label, width, height);
	}

	public static Rect DrawLabelInfo(Camera cam, Vector3 labelWorldPosition,GUIContent content, GUIStyle style, int width, int height)
	{
		TextAnchor defaultLblAlig = GUI.skin.label.alignment;
		GUI.skin.label.alignment=TextAnchor.MiddleCenter;

		Rect labelRect = GetDrawLabelRect(cam,labelWorldPosition,width,height);

		GUI.Label(labelRect,content,style);

		GUI.skin.label.alignment = defaultLblAlig;
		return labelRect;
	}

	public static Rect DrawLabelInfo(Camera cam, Vector3 labelWorldPosition,Vector3 pointerWorldPosition,GUIContent content, int width, int height, Color lineColor)
	{
		return DrawLabelInfo (cam, labelWorldPosition, content, GUI.skin.label, width, height);
	}

	public static Rect DrawLabelInfo(Camera cam, Vector3 labelWorldPosition,Vector3 pointerWorldPosition,GUIContent content, GUIStyle style, int width, int height, Color lineColor)
	{
//		TextAnchor defaultLblAlig = GUI.skin.label.alignment;
//		GUI.skin.label.alignment=TextAnchor.MiddleCenter;


		Rect labelRect = GetDrawLabelRect(cam,labelWorldPosition,width,height);

		Rect pointerRect = GetDrawLabelRect(cam,pointerWorldPosition,width,height);

		GUI.Label(labelRect,content,style);

		if (pointerRect.y - labelRect.y > 50f)
		{

			Drawing.DrawLine(
				new Vector2(labelRect.x + labelRect.width/4f,labelRect.center.y),
				new Vector2(pointerRect.center.x - pointerRect.width/8f,pointerRect.center.y),
				lineColor,2f);
			Drawing.DrawLine(
				new Vector2(pointerRect.center.x - pointerRect.width/8f,pointerRect.center.y),
				pointerRect.center,
				lineColor,2f);

			Drawing.DrawLine(
				new Vector2(labelRect.x + labelRect.width/4f,labelRect.center.y),
				new Vector2(pointerRect.center.x - pointerRect.width/8f,pointerRect.center.y),
				lineColor,2f);
			Drawing.DrawLine(
				new Vector2(pointerRect.center.x - pointerRect.width/8f,pointerRect.center.y),
				pointerRect.center,
				lineColor,2f);
		}
		//		Vector3 otherCenterPostion = ObservingCamera.WorldToScreenPoint(pointerWorldPosition);
		//		Vector3 otherCenterPostion2 = otherCenterPostion;
		//		otherCenterPostion.x = Mathf.Lerp(otherCenterPostion2.x,labelRect.x,0.2f);
		//		
		//		if (Mathf.Abs(lblPos.y - otherCenterPostion.y)>=10f)
		//		{
		//			
		//			Drawing.DrawLine(
		//				new Vector2(Mathf.Clamp(lblPos.x-50f,0,ObservingCamera.pixelWidth),ObservingCamera.pixelHeight - lblPos.y),
		//				new Vector2(otherCenterPostion.x,ObservingCamera.pixelHeight - otherCenterPostion.y),
		//				lineColor,2f);
		//			Drawing.DrawLine(
		//				new Vector2(otherCenterPostion.x,ObservingCamera.pixelHeight - otherCenterPostion.y),
		//				new Vector2(otherCenterPostion2.x,ObservingCamera.pixelHeight - otherCenterPostion2.y),
		//				lineColor,2f);
		//		}
//		GUI.skin.label.alignment = defaultLblAlig;
		return labelRect;
	}

	/// <summary>
	/// On value changed delegate to perform validiation.
	/// </summary>
	/// <param name="obj">The object that has changed value.</param>
	/// <param name="fieldLabel">The label object of the changed field.</param>
	/// <param name="control">The UI component that call the value change.</param>
	public delegate void OnValueChanged(object obj, object value, Text fieldLabel, Object control);


	/// <summary>
	/// User interface prefabs.
	/// </summary>
	[System.Serializable]
	public class UIPrefabs
	{
		public Button _buttonPrefab;
		public Text _textPrefab;
		public InputField _inputFieldPrefab;
		public Slider _sliderPrefab;
		public Dropdown _dropDownPrefab;
		public Toggle _togglePrefab;
	}





	/// <summary>
	/// List of the user interface prefabs.
	/// </summary>
	private static UIPrefabs uiPrefabs = null;

	/// <summary>
	/// Sets the user interface prefabs that will be used to generate auto UI.
	/// </summary>
	/// <param name="prefabs">Prefabs.</param>
	public static void SetUIPrefabs (params object[] prefabs)
	{
		if (uiPrefabs != null)
			uiPrefabs = new UIPrefabs ();
		for (int i = 0;i<prefabs.Length;i++)
		{
			if (prefabs[i]!=null)
			{
				System.Type prefabType = prefabs [i].GetType ();
				if (typeof(Button).IsAssignableFrom (prefabType))
				{
					uiPrefabs._buttonPrefab = (Button) prefabs [i];
				}
				if (typeof(Text).IsAssignableFrom (prefabType))
				{
					uiPrefabs._textPrefab = (Text) prefabs [i];
				}
				if (typeof(InputField).IsAssignableFrom (prefabType))
				{
					uiPrefabs._inputFieldPrefab = (InputField) prefabs [i];
				}
				if (typeof(Slider).IsAssignableFrom (prefabType))
				{
					uiPrefabs._sliderPrefab = (Slider) prefabs [i];
				}
				if (typeof(Dropdown).IsAssignableFrom (prefabType))
				{
					uiPrefabs._dropDownPrefab = (Dropdown) prefabs [i];
				}
				if (typeof(Toggle).IsAssignableFrom (prefabType))
				{
					uiPrefabs._togglePrefab = (Toggle) prefabs [i];
				}
			}
		}
	}



	#endregion


}

